#!/usr/bin/lua
if tonumber((string.gsub(_VERSION, "Lua ", ""))) < 5.2 then error("Require Lua 5.2"); end
local _FILENAME = "madm.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170830"

local GLOBAL_CONFIG = "/etc/madm.conf.lua"

local arg, package = arg, package
local pairs, require, tonumber = pairs, require, tonumber
local io = {open = io.open, write = io.write}
local os = {exit = os.exit, getenv = os.getenv}
local string = {find = string.find, format = string.format, gsub = string.gsub, lower = string.lower, match = string.match, sub = string.sub}
local table = {concat = table.concat}
_ENV = nil

local getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

local t_arg = {}
for i = 1, #arg do
  if arg[i - 1] == "-F" then
    t_arg.s_global_config = arg[i]
  elseif arg[i - 1] == "-f" then
    t_arg.s_user_config = arg[i]
  elseif arg[i - 1] == "-s" then
    t_arg.s_response_value_separator = arg[i]
  elseif arg[i - 1] == "-c" then
    t_arg.connection_data_id = tonumber(arg[i]) or arg[i] or 1
  elseif not(string.find(arg[i], "%-[Ffsc]$")) then
    t_arg[#t_arg + 1] = arg[i]
  end
end
t_arg.s_global_config = t_arg.s_global_config or GLOBAL_CONFIG

-- Get LUA_PATH/LUA_CPATH from global_conf
local f_global_config, s_err = io.open(t_arg.s_global_config)
if f_global_config then
  for line in f_global_config:lines() do
    string.gsub(line, "^LUA_(C?)PATH *= *\"?([^\"]*)\"?$", function(is_c, is_path)
      local s_path = is_c == "C" and "cpath" or "path"
      package[s_path] = string.gsub(is_path, ";;", string.format(";%s", package[s_path]))
    end)
  end
  f_global_config:close()
end

local tool = require("madm.tool")
local t_request = tool.init(t_arg)
if not(tool.t_config.DATA_MASTER_PASSWORD) and not(t_request.action == "--install") then
  io.write("ERROR = \"DATA_MASTER_PASSWORD is required.\"\n")
  os.exit(1)
end
if tool.t_config.runas_user then
  local b_res, s_err = tool.chuser(tool.t_config.runas_user)
  if not(b_res) then
    io.write(string.format("ERROR = \"Change user to %s: %s\"\n", tool.t_config.runas_user, s_err))
    os.exit(1)
  end
end

if os.getenv("GATEWAY_INTERFACE") then
  local server = tool.require("madm.server")
  server.doCGIRequest()
elseif (tool.t_config.scgi_port) then
  local socket = tool.require("socket")
  local sbind, s_err = socket.bind(tool.t_config.scgi_host or "127.0.0.1", tool.t_config.scgi_port)
  if s_err then
    tool.log.ERROR("Cannot bind scgi server: %s", s_err)
    io.write(string.format("ERROR = Cannot bind scgi server: %s\n", s_err))
    os.exit(1)
  end
  local server = tool.require("madm.server")
  while 1 do
    local scgi_sock_io, s_err = sbind:accept()
    if s_err then
      tool.log.ERROR("SCGI Server accept fail: %s", s_err)
    else
      server.doCGIRequest(scgi_sock_io)
      scgi_sock_io:close()
    end
  end
elseif tool.t_config.madm_socket or tool.t_config.dovecot_auth_socket then
  local socket_server = tool.require("madm.socket_server")
  socket_server.run()
elseif not(t_request.action) then
  local client_shell = tool.require("madm.client_shell")
  client_shell.run()
elseif (t_request.action == "--active") or (t_request.action == "--inactive") then
  local s_key
  local t_object = tool.checkUserDomain(t_request.object)
  if t_object.s_domain then
    if t_object.s_user then
      s_key = tool.getRedisKey("domain_user_list", t_object)
    else
      s_key = tool.getRedisKey("config_domain_list")
    end
  end
  if s_key then
    local r = tool.require("redis").connect(tool.t_config.s_redis_dsn, tool.redis_log)
    if r then
      local t_result = r.q({{"SISMEMBER", s_key, t_object.s_object}})[1]
      if t_result[1] == 0 then
        io.write(string.format("ERROR = Object: \"%s\" does not exist.\n", t_request.object))
      elseif t_result[1] == 1 then
        local s_set_inactive = string.match(t_request.action, "in")
        local t_result = r.q({{s_set_inactive and "SADD" or "SREM", string.format("%s_INACTIVE", s_key), t_object.s_object}})[1]
        io.write(string.format("Object: \"%s\" %s %sactive.\n", t_request.object, t_result[1] == 1 and "set" or "is", s_set_inactive or ""))
      end
      r.close()
    end
  else
    io.write(string.format("Object: \"%s\" is invalid.\n", t_request.object or ""))
  end
elseif t_request.action == "--checkUserDomain" then
  local t_object = tool.checkUserDomain(t_request.object) or {}
  for k, v in pairs(t_object) do io.write(string.format("%s = \"%s\"\n", k, v)); end
  io.write(string.format("-\ns_object = \"%s\"\n", t_object.s_object or "nil"));
elseif t_request.action == "--fetch-tlds" then
  local s_fetch_tld_list = tool.getTldsAlphaByDomain()
  if s_fetch_tld_list then
    io.write(string.format("Put the file \"%s\" into your LUA_PATH directory.\n -> LUA_PATH=\"%s\"\n", s_fetch_tld_list, package.path))
  end
elseif t_request.action == "--install" then
  local install = tool.require("madm.install")
  install.run()
elseif (t_request.action == "--offline") or (t_request.action == "--online") then
  local r = tool.require("redis").connect(tool.t_config.s_redis_dsn, tool.redis_log)
  if r then
    local s_onoff = string.match(t_request.action, "off")
    local t_result = r.q({{"SET", tool.getRedisKey("config_online"), s_onoff and 0 or 1}})[1]
    if t_result[1] then
      io.write(string.format("%s set %sline.\n", tool.t_config.s_program_name, s_onoff or "on"))
    else
      io.write(string.format("ERROR = Cannot set status: %s.\n", t_result[2]))
    end
    r.close()
  end
elseif t_request.action == "-p" then
  local s_setpass = t_request.attributes
  local s_pass = tonumber(t_request.object) and tool.generatePassword(tonumber(t_request.object)) or t_request.object or tool.generatePassword()
  if t_request.object and (t_request.object == "--setpass") then
    s_setpass = t_request.object
    s_pass = tool.generatePassword()
  end
  local s_pass_crypt = tool.crypt(s_pass, tool.t_config.DATA_MASTER_PASSWORD, tool.ENCRYPT, "x")
  io.write(string.format("%s = %s\n", s_pass, s_pass_crypt))
  if s_setpass and (s_setpass == "--setpass") then
    local t_object = tool.checkUserDomain(t_request.values or t_request.attributes or "")
    local s_key = tool.getRedisKey(t_object.s_user and "user_pass" or t_object.s_client and "config_client_pass" or "", t_object)
    if s_key then
      local r = tool.require("redis").connect(tool.t_config.s_redis_dsn, tool.redis_log)
      if r then
        local t_result = r.q({{"EXISTS", s_key}})[1]
        if t_result[1] == 1 then
          t_result = r.q({{"SET", s_key, s_pass_crypt}})[1]
        end
        io.write(string.format("Password set: %s\n", t_result[1] or t_result[2]))
        r.close()
      end
    end
  end
elseif t_request.action == "--dest-stat" then
  local r = tool.require("redis").connect(tool.t_config.s_redis_dsn, tool.redis_log)
  local t_data = {s_user = "*", s_domain = "*"}
  if r then
    local t_user = r.q({{"KEYS", tool.getRedisKey("user_created", t_data)}})[1]
    local t_request = {{"MGET"}}
    local t_result = {}
    for n_k, s_v in pairs(t_user) do
      t_result[n_k] = {string.gsub(s_v, ".*DOMAIN@([^@]+)@USER@([^@]+)@.*", "%2@%1"), -1}
      t_request[1][#t_request[1] + 1] = (string.gsub(s_v, "CREATED$", "MAILBOX"))
      t_request[#t_request + 1] = {"SMEMBERS", (string.gsub(s_v, "CREATED$", "ALIASES"))}
    end
    local t_user_data = r.q(t_request)
    for n_k, s_v in pairs(t_user) do
      t_result[n_k][2] = t_user_data[1][n_k]
      t_result[n_k][3] = t_user_data[n_k + 1]
    end
    local n_mailbox, n_alias = 0, 0
    for _, s_v in pairs(t_result) do
      io.write(string.format("%s%s%s\n", s_v[1], s_v[2] == "1" and "+" or "-", table.concat(s_v[3] or {}, ", ")))
      if s_v[2] == "1" then
        n_mailbox = n_mailbox + 1
      end
      n_alias = n_alias + #(s_v[3] or {})
    end
    io.write(string.format("--\nUser: %d, Mailbox: %d, Alias: %d\n", #t_result, n_mailbox, n_alias))
  end
elseif t_request.action == "-v" then
  io.write(string.format("%s %s %s\n", getVersion()))
  for _, t_module in pairs(tool.t_config.t_module_internal) do
    local m = tool.require(t_module[1], "INFO")
    if m then
      io.write(string.format("|-%s (%s)\n", table.concat({m.getVersion()}, " "), t_module[2]))
    end
  end
else
  if not(tool.t_config.t_action[t_request.action]) then
    if not(string.find(t_request.action, "^%-h")) then
      t_request = tool.getConfFileData(t_request.action) or {}
      t_request.action = string.lower(t_request.action or "")
    end
    if not(tool.t_config.t_action[t_request.action]) then
      local help = tool.require("madm.help", "WARN")
      if help then
        io.write(help.write(string.sub(t_request.action, 3)))
      end
      os.exit(string.find(t_request.action, "^%-h") and help and help.write and 0 or 1)
    end
  end
  local client = tool.require("madm.client")
  local t_result = client.doRequest(t_request) or {status = 0, error = {message = "No result"}}
  io.write(table.concat(client.formatResult(t_result)))
  os.exit((t_result.status == 1) and 0 or 1)
end

