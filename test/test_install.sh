#!/bin/bash
REDIS_DSN=${1:-redis://}

dsn="${REDIS_DSN#redis://}"
hostname="${dsn%%[:/]*}"
test -z "${hostname}" && hostname="127.0.0.1"
port=6379
dbnum=1
if [ "${dsn/:/}" != "${dsn}" ]; then
  port="${dsn#*:}"
fi
if [ "${dsn/\//}" != "${dsn}" ]; then
  dbnum="${dsn#*/}"
  port="${port%/*}"
fi

SIZE=$(echo "DBSIZE" | redis-cli -h ${hostname} -p ${port} -n ${dbnum} 2>&1)

if [ "Could not connect to" = "${SIZE%% Redis *}" ]; then
  echo "ERROR - Could not connect to redis://${hostname}:${port}/${dbnum}"
  exit 1
elif [ "0" != "${SIZE}" ]; then
  echo "ERROR - Database contains data."
  exit 1
fi

cat << EOF | madm --install
${REDIS_DSN}




user

N

EOF

echo -e "\nDB exists..\n"

cat << EOF | madm --install
${REDIS_DSN}
y




user

N

EOF

echo -n "\nRemove DB: "
echo "FLUSHDB" | redis-cli -h ${hostname} -p ${port} -n ${dbnum}

