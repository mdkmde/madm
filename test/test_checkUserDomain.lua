local _FILENAME = "test_checkUserDomain.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170626"

t = require("madm.tool")
t.genConfig()
x = require("test")

local s_request = ""
local t_result = {
  s_user = "nil",
  s_domain = "nil",
  s_user_domain = "nil",
  s_client = "nil",
  s_cast = "nil",
  s_type = "config",
  s_object = "nil",
}

local run = function() x.run(s_request, t.checkUserDomain, {s_request}, {t_result}); end
--check casts
run()

s_request = "normal_string_value"
run()

s_request = "user@domain.com"
t_result.s_user = "user"
t_result.s_domain = "domain.com"
t_result.s_user_domain = "user@domain.com"
t_result.s_type = "user"
t_result.s_object = "user@domain.com"
run()

s_request = "(user)user@domain.com"
t_result.s_cast = "u"
run()
s_request = "(u)user@domain.com"
run()

s_request = "(d)user@domain.com"
t_result.s_cast = "d"
t_result.s_type = "domain"
t_result.s_object = "domain.com"
run()

s_request = "(c)user@domain.com"
t_result.s_user = "nil"
t_result.s_domain = "nil"
t_result.s_user_domain = "nil"
t_result.s_cast = "c"
t_result.s_type = "config"
t_result.s_object = "nil"
run()

s_request = "(c)client"
t_result.s_client = "client"
t_result.s_object = "client"
run()

s_request = "(c)client@"
run()

--simple input
s_request = "client@"
t_result.s_cast = "nil"
run()

s_request = "domain.com"
t_result.s_client = "nil"
t_result.s_domain = "domain.com"
t_result.s_type = "domain"
t_result.s_object = "domain.com"
run()

s_request = "user@domain.com"
t_result.s_user = "user"
t_result.s_domain = "domain.com"
t_result.s_user_domain = "user@domain.com"
t_result.s_type = "user"
t_result.s_object = "user@domain.com"
run()

--input fail
s_request = "invalid..user@domain.com"
t_result.s_user = "nil"
t_result.s_user_domain = "nil"
t_result.s_type = "domain"
t_result.s_object = "domain.com"
run()

s_request = "(u)invalid..user@domain.com"
t_result.s_user = "nil"
t_result.s_user_domain = "nil"
t_result.s_cast = "u"
t_result.s_type = "domain"
t_result.s_object = "nil"
run()

s_request = "user@invalid..domain.com"
t_result.s_user = "user"
t_result.s_domain = "nil"
t_result.s_user_domain = "nil"
t_result.s_cast = "nil"
t_result.s_type = "config"
t_result.s_object = "nil"
run()

s_request = "(d)user@invalid..domain.com"
t_result.s_cast = "d"
run()

s_request = "(u)user@invalid..domain.com"
t_result.s_cast = "u"
run()
s_request = "(c)user@invalid..domain.com"
t_result.s_user = "nil"
t_result.s_cast = "c"
run()

s_request = ""
t_result.s_cast = "nil"
run()

x.summary()

