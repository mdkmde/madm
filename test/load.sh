#!/bin/bash
# load.sh [parallel [serial]]

SOCKET="./exim_madm_socket"
USER="test@example.com"
call() {
#  exim -be "\${readsocket{${SOCKET}}{separator = \":\", action = \"get\", object = \"\", attributes = \{\"domain_list_active\"\}\\n}}"
  exim -be "\${readsocket{${SOCKET}}{action = \"get\", object = \"${USER}\",  attributes = \{\"recipient_data\"\}, values = \{\"update_last_access\"\}\\n}}"
}

if [ -z "${1}" ]; then
  call
  exit 0
fi

PL=${1}
SL=${2:-1}
pids=""

date
for i in $(seq ${PL}); do
  (for j in $(seq ${SL}); do call; done | grep -v recipient_data | grep -v "^[ \t]*$") &
  pids="${pids} $!"
done
wait ${pids}
date

