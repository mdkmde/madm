local _FILENAME = "madm/socket_server.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20181016"

local _M = {}
local load, pairs, pcall, require, tonumber, type = load, pairs, pcall, require, tonumber, type
local math = {floor = math.floor}
local os = {date = os.date}
local string = {find = string.find, format = string.format, gsub = string.gsub}
local table = {concat = table.concat}
local tool = require("madm.tool")
local client = require("madm.client")
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

local load_cache, redis_cache

local checkUserAutoDomainSuffix = function(is_object)
  local s_object = string.gsub(is_object, "^%(u%)([^@]*)$", "%1")
  if not(s_object == is_object) and type(tool.t_config.auto_domain_suffix) == "table" then
    for s_k, t_v in pairs(tool.t_config.auto_domain_suffix) do
      if t_v[s_object] then
        return string.format("(u)%s@%s", s_object, s_k)
      end
    end
  end
  return is_object
end

local doRequest_madm = function(is_request, it_request)
  local s_request = string.format("return { %s }", is_request)
  local s_cache_result = string.find(s_request, "action[^,]+get") and redis_cache.get(s_request)
  local errorInvalidRequest = function(is_request, is_err)
    tool.log.WARN("Lua load error: \"%s\" : \"%s\"", is_request, is_err)
    redis_cache.set(is_request, "ERROR = invalid request\n", tool.t_config.n_socket_server_redis_cache_load_err_ttl)
    return "ERROR = invalid request\n"
  end
  if not(s_cache_result) then
    local cacheLoadFunction = load_cache.get(s_request)
    if not(cacheLoadFunction) then
      local b_success, loadFunction, s_err = pcall(load, s_request)
      if b_success and loadFunction then
        cacheLoadFunction = loadFunction
        load_cache.set(s_request, cacheLoadFunction)
      else
        errorInvalidRequest(s_request, b_success and s_err or loadFunction)
      end
    end
    local b_success, t_request = pcall(cacheLoadFunction)
    if b_success then
      t_request.rid = t_request.rid or it_request.rid
      t_request.object = t_request.object or ""
      t_request.object = checkUserAutoDomainSuffix(t_request.object)
      local t_result = client.doRequest(t_request) or {status = 0, error = {message = "No result"}}
      s_cache_result = table.concat(client.formatResult(t_result, t_request.separator))
      redis_cache.set(s_request, s_cache_result, tool.t_config.n_socket_server_redis_cache_ttl)
    else
      errorInvalidRequest(s_request, t_request)
    end
  end
  return s_cache_result
end

local json
local doRequest_dovecot_auth = function(is_request, it_request)
  local s_cmd = is_request:sub(1, 1)
  if  s_cmd == "L" then
    it_request.object = is_request:gsub("Lshared/", "(u)")
    it_request.object = checkUserAutoDomainSuffix(it_request.object)
    local s_cache_result = redis_cache.get(it_request.object)
    if not(s_cache_result) then
      local t_result = client.doRequest(it_request)
      if (t_result.status == 1) and (t_result.data.pass) then
        for _, s_limit in pairs({"mailbox_limit", "mailbox_limit_raw", "mailbox_limit_warn", "mailbox_limit_warn_raw"}) do
          t_result.data[s_limit] = t_result.data[s_limit] or "0"
        end
        local t_result_dovecot = {}
        for s_k, s_v in pairs(tool.t_config.result or t_result.data) do
          if not(type(s_v) == "string") then
            tool.log.ERROR("doRequest_dovecot_auth: Unknown return value for \"%s\" = \"%s\" (%s)", s_k, t_result_dovecot[s_k], type(t_result_dovecot[s_k]))
          else
            t_result_dovecot[s_k] = string.gsub(s_v, "%${([^}]+)}", t_result.data)
          end
          tool.log.DEBUG("result: \"%s\" = \"%s\"", s_k, t_result_dovecot[s_k])
        end
        s_cache_result = string.format("O%s\n", json.encode(t_result_dovecot))
      else
        s_cache_result = "N\n"
      end
      redis_cache.set(it_request.object, s_cache_result, tool.t_config.n_socket_server_redis_cache_ttl)
    end
    return s_cache_result
  elseif not(s_cmd == "H") and not(s_cmd == "") then
    return "F\n"
  end
end

local t_do_request = {
  madm = doRequest_madm,
  dovecot_auth = doRequest_dovecot_auth,
}

_M.run = function()
  local cache = tool.require("cache")
  load_cache = cache.init(tool.t_config.n_socket_server_load_cache_size)
  redis_cache = cache.init(tool.t_config.n_socket_server_redis_cache_size)
  local s_request_type = "madm"
  local s_socket = tool.t_config.madm_socket
  local u_socket = require("socket.unix")()
  local t_request = {action = "", object = "", rid = ""}
  if tool.t_config.madm_socket then
    local _THIS_IS_PRIMARY
  elseif tool.t_config.dovecot_auth_socket then
    s_request_type = "dovecot_auth"
    s_socket = tool.t_config.dovecot_auth_socket
    json = tool.require("json")
    t_request.action = "get"
    t_request.attributes = {"user", "pass", "mailbox_data", "mailbox_limit"}
    t_request.values = {"update_last_access"}
  end
  tool.unlink(s_socket)
  u_socket:bind(s_socket)
  u_socket:listen(tool.t_config.n_socket_server_backlog)
  local n_proxy_rid = tonumber((string.gsub(os.date("!%Y%j%T0001"), "([0-2][0-9]):([0-5][0-9]):([0-5][0-9])", function(in_h, in_m, in_s) return string.format("%05d", math.floor((in_h * 60 * 60) + (in_m * 60) + in_s)); end)))
  while 1 do
    local c_socket = u_socket:accept()
    while 1 do
      local s_request, s_err = c_socket:receive("*l")
      if s_err then
        if not(s_err == "closed") then
          tool.log.WARN("Socket read error: \"%s\"", s_err)
        end
        c_socket:close()
        break
      else
        tool.log.DEBUG("Received socket request: \"%s\"", s_request)
        t_request.rid = string.format("%d", n_proxy_rid)
        n_proxy_rid = n_proxy_rid + 1
        local s_reply = t_do_request[s_request_type](s_request, t_request)
        if s_reply then
          tool.log.DEBUG("Send socket reply: \"%s\"", s_reply)
          c_socket:send(s_reply)
        end
      end
    end
  end
end

return _M

