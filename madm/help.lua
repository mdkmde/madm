local _FILENAME = "madm/help.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170627"

local _M = {}
local pairs = pairs
local io = {write = io.write}
local string = {find = string.find, format = string.format, gsub = string.gsub}
local table = {concat = table.concat, sort = table.sort}
local tool = require("madm.tool")
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

_M.t_shell_help = {string.format([=[

%s - client shell command list
%s============================
c [object|-|..]                   change object to domain, user or client
l [-a[x]|[<>0-9]*o|c] [<filter>]  list (a)ttribute, (o)bject, (c)lient (with <filter>)
                                  <filter> => (part of )(a|o|c)-key (only single filter supported)
                                  [0-9]*o => add sum if greater or equal than n objects / "-0o" will sum up only
                                  show "<" = inactive / ">" = active objects only
                                  default shell list option: "%s"

h                                 help
q                                 exit

new object                        create new domain or user
del object                        delete domain or user (delete user mailbox in filesystem as well)

attribute [attribute [..]]        get attribute (attr* => "l -ax attr")
attribute = <value>               set attribute to value (<value>[ <value>[ ..]])
                                  ! No quoting supported - separator is " " and could be changed with:
                                  attribute =|= var 1|var 2|..
                                  e.g.: (password has to contain a number an upper case and a lower case letter)
                                  password_condition =   ^.*[0-9].*$ ^.*[A-Z].*$ ^.*[a-z].*$
                                  password_condition =|= ^.*[0-9].*$|^.*[A-Z].*$|^.*[a-z].*$

connection_data [= <id>]          show/use connection_data (see: "-hc" -> help config files)
]=],
  tool.t_config.s_program_name,
  string.gsub(tool.t_config.s_program_name, ".", "="),
  tool.t_config.s_shell_list_option
)}

_M.write = function(is_topic)
  if (is_topic == "a") or (is_topic == "ax") then
    local lsAttribute = function(is_type)
      local t_out = {}
      for s_k, _ in pairs(tool.t_config.t_redis_key) do
        string.gsub(s_k, string.format("^%s_(.*)", is_type), function(is_attribute)
          t_out[#t_out + 1] = is_attribute
        end)
      end
      table.sort(t_out)
      return table.concat(t_out, "|")
    end

    is_topic = string.format([=[

%s - API-DOC
%s==========

Request:
========
HTTP_DATA:
----------
request=JSON_OBJECT
hash=HASH

JSON_OBJECT:
------------
Required:
---------
ruser = "user@domain"
rhash_algo = "sha1|md5|.."
rhash_armor = "x|X|b" (hex|HEX|base64)
rclient = "client_name"
rid = REQUEST_ID = inc number
action = "new|set|get|mov|del"

Optional:
---------
object = "[user@]domain"

attributes =
  [config] %s
  [domain] %s
  [user] %s

values = "string" | number | ["+|-", "string", "string", "+|-", ..]

user_admin_flags = %s

HASH:
-----
hmac_hash $request $algo $armor $pass

algo = md5|sha1|..
armor = x(hex)|X(HEX)|b(base64)
pass = "$client_pass$user_pass"
%s
]=],
      tool.t_config.s_program_name,
      string.gsub(tool.t_config.s_program_name, ".", "="),
      lsAttribute("config"),
      lsAttribute("domain"),
      lsAttribute("user"),
      (function()
        local t_out = {}
        for s_k, _ in pairs(tool.t_config.t_admin_flag) do
          t_out[#t_out + 1] = s_k
        end
        table.sort(t_out)
        return table.concat(t_out, "|")
      end)(),
      (is_topic == "ax") and [=[


Examples:
---------
User exist:
action = get
object = user@example.com
attributes = ["active"]
=> empty data (or "data": {"active": 0 -> inactive / 1 -> active})

Move user:
action = mov
object = user@example.com
attributes = ["dest", "redirect_mails"]
values = ["user@example2.com", 1]
=> ???

Create user:
action = new
object = user@example.com
=> emtpty data (on failure: "status": 0, {"error": {"message": "ERROR"})

Set user data:
action = set
object = user@example.com
attributes = ["pass", "mailbox", "aliases"]
values = ["eXample01", 1, ["alias1@example.com", "alias2@example.com"]]
=> emtpty data

Get user data:
action = set
object = user@example.com
attributes = ["mailbox", "aliases"]
=> "data": {"mailbox": 1, "aliases": ["alias1@example.com", "alias2@example.com"]}
]=] or ""
    )
  elseif (is_topic == "c") or (is_topic == "cx") then
    is_topic = string.format([==[

%s - CONFIG FILES:
%s================
local nolog = function() end
log = setmetatable({}, {__index = function() return nolog end})
cgi_log = log
redis_log = log
scgi_host = "127.0.0.1"
-- if port exists - start SCGI server
scgi_port = 8000
-- if socket is defined starts dovecot auth proxy
dovecot_auth_socket = "/tmp/dove_auth"
madm_socket = "/tmp/madm_sock"
response_value_separator = "%s"
runas_user = "vmail"
result = {
  user = "${user}",
  password = "${pass}",
  userdb_uid = "2000",
  userdb_gid = "2000",
  userdb_home = "${mailbox_data}",
  userdb_mail = "${mailbox_data_type}:${mailbox_data}",
  userdb_quota_rule = "*:bytes=${mailbox_limit_raw}"
}

GLOBAL_CONF (%s)
-------------%s-
%s
-- advanced config to extend keys
redis_key_addons = {
  -- {permission, value_type, default_value, delete_value, bool_value, redis_key}
  -- nil on bool_value => neither bool nor any other -> read_only
  -- "_domain" and "_user" will be replaced - for more examples see t_redis_key in %s
  domain_greylist = {"domain", "number", 1, 0, true, {"DOMAIN", "_domain", "GREYLIST"}},
  user_daily_send_limit = {"domain", "number", 100, 0, false, {"DOMAIN", "_domain", "USER", "_user", "DAILY_SEND_LIMIT"}},
}


USER_CONF (%s)
-----------%s-
--Overwrite GLOBAL_CONF
--DATA_MASTER_PASSWORD = [=[CLIENT_PASSWORD]=]
-- set for remote requests
--server_url = <remote_url>
--[[ <remote_url>  => "https?://localhost/madm.cgi"
-- (connection data: "server_url/user/user_pass" may be a
-- "table" to handle multi connection data:
-- {"default_remote_url", id = "remote_url", localhost = true}
-- [1] is default/fallback (localhost, if not set))
--]]
user = "me@example.com"
-- create with "-p" option (set DATA_MASTER_PASSWORD first)
user_pass = ""
client = "myclient"
client_pass = ""
response_value_separator = "%s"
shell_list_option = "%s"
]==],
      tool.t_config.s_program_name,
      string.gsub(tool.t_config.s_program_name, ".", "="),
      tool.t_config.response_value_separator,
      tool.t_config.s_global_config,
      string.gsub(tool.t_config.s_global_config, ".", "-"),
      string.format(tool.t_config.s_global_config_template,
        "SERVER_PASSWORD",
        tool.t_config.s_redis_dsn,
        tool.t_config.s_redis_namespace
      ),
      tool.t_config.s_program_name,
      tool.t_config.s_user_config,
      string.gsub(tool.t_config.s_user_config, ".", "-"),
      tool.t_config.response_value_separator,
      tool.t_config.s_shell_list_option
    )
  elseif is_topic == "m" then
    is_topic = string.format([[

%s - MAIL SERVER SPECIAL REQUEST:
%s===============================
mail server users should have these admin_flags:
"global_domain" "read_only" "read_data" "read_pass"

For smtp server:
----------------
action = "get"
attributes = {"recipient_data"} (requires admin_flag "read_data")
[values = {"limit_mail<sender@example.com>"}|{"update_last_access"}]

returns a complete resolved mailpath:
recipient_data = "/home/user/.Maildir, alias@otherdomain.example.com"
[limit_subject = "Your mailbox gets over limit"] (in case of limit_warn)
[limit_mail = 1] (No user warn limit info was sent yet)

or if mailbox is over limit:
recipient_data = "alias@otherdomain.example.com" (without user mailbox)
limit_over_limit = 1
limit_subject = "Your mailbox is over limit"
[limit_mail = 1] (No user over limit info was sent yet)

if the "limit_mail" value is set, on the first hit of the warn/limit mark
the limit mail data is returned as well:
limit_sender = "postmaster@userdomain.example.com"
limit_text = "Mailbox full body"

--

action = "get"
attributes = {"autore_active"}
[values = {"sender@example.com"}]

returns:
[autore_active = 1]

if "sender" value is given, "sender" is added to an internal list and
the next call with these "sender" won't return an "autore_active" result.
(list reset is on reset of "autore_start" or if "autore_end" is reached)

You should fetch: "autore_active", "autore_reply_as", "autore_subject", "autore_text"

--

For imap/pop3 server:
---------------------
action = "get"
attributes = {"mailbox_data"} (requires admin_flag "read_data")
[values = {"update_last_access"}]

returns:
[mailbox_data = "/home/user/.Maildir"]
[mailbox_data_limit = "1M"]
[mailbox_data_limit_raw = "1024"]
[mailbox_data_type = "%s"]
[limit_over_limit = 1]
[limit_subject = ""]

if "update_last_access" value is given, the "last_access" of the user is set.

]],
      tool.t_config.s_program_name,
      string.gsub(tool.t_config.s_program_name, ".", "="),
      table.concat(tool.t_config.t_mailbox_type, "|")
    )
  elseif is_topic == "r" then
    io.write("\n")
    for t_k, t_v in pairs(tool.t_config.t_module) do
      if tool.require(t_k, "!MISS") then
        io.write(string.format("   OK: %s\n", t_v[2]))
      end
    end
    is_topic = "\n"
  elseif is_topic == "s" then
    is_topic = string.format("%s\n", _M.t_shell_help[1])
  else
    is_topic = string.format([[

%s - help
%s=======
-h                    help
-ha                   help API
-hax                  help API + example
-hc                   help config files
-hm                   help mail server special request
-hr                   help required modules
-hs                   help client shell
-v                    show version info

-F <file>             global config file (%s)
-f <file>             user config file (%s)
-s <separator>        response value separator ("%s")
-c <id>               use connection data id (see: "-hc" -> help config files)

-p [n|<pass>]         return password hash for user config file
                      if empty pass, generates pass with length n or %s
                      -p [n|<pass>][ --setpass <user|client>]

--[in]active <object> set <object> (user|domain) (in)active
--install             install madm on redis instance
--fetch-tlds          fetch tlds-alpha-by-domain list for domain checks
--on/offline          set online or offline

<request>             => <request_file> | action object attributes values
                      request_file:
                      action = "get|set|new|mov|del"
                      object = ""
                      attributes = {}
                      values = {}

]],
      tool.t_config.s_program_name,
      string.gsub(tool.t_config.s_program_name, ".", "="),
      tool.t_config.s_global_config,
      tool.t_config.s_user_config,
      tool.t_config.response_value_separator,
      tool.t_config.t_redis_key["config_password_min_length"][3],
      tool.t_config.s_program_name,
      tool.t_config.s_program_name
    )
  end
  return is_topic
end

return _M

