local _FILENAME = "madm/install.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170627"

local _M = {}
local pairs = pairs
local io = {open = io.open, write = io.write}
local os = {exit = os.exit, getenv = os.getenv}
local string = {find = string.find, format = string.format, gsub = string.gsub, lower = string.lower, sub = string.sub}
local LUA_PATH = package.path
local tool = require("madm.tool")
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

_M.run = function()
  io.write("Check required libraries.. ")
  local socket = tool.require("socket")
  local redis = tool.require("redis")
  local cgi = tool.require("cgi", "  WARN")
  local json = tool.require("json", "  WARN")
  local log = tool.require("log", "NOTICE")
  local r, s_err = nil, "OK\nSet up redis connection, please provide a redis DSN:\nredis://[auth@][server][:port][/db]"
  local s_redis_dsn
  while s_err do
    s_redis_dsn = tool.readline(string.format("%s\nSet up first admin config: redis_dsn [%s]> ", s_err, tool.t_config.s_redis_dsn)) or "q"
    if s_redis_dsn == "q" then io.write("\n"); os.exit(0) end
    s_redis_dsn = (s_redis_dsn == "") and tool.t_config.s_redis_dsn or s_redis_dsn
    r, s_err = redis.connect(s_redis_dsn)
  end
  if 0 < r.q({{"DBSIZE"}})[1][1] then
    if not(string.find(string.lower(tool.readline("The redis db contains data. continue? [y/N]> ")), " *y *")) then
      io.write("\n")
      os.exit(0)
    end
  end
  local s_master_password = tool.generatePassword(20)
  local t_setup = {
    s_redis_namespace = tool.t_config.s_redis_namespace,
    s_client = "localhost",
    s_client_pass = tool.generatePassword(10),
    s_domain = "localhost",
    s_user = "",
    s_user_pass = tool.generatePassword(10),
  }
  for _, v in pairs({"s_redis_namespace", "s_client", "s_client_pass", "s_domain", "s_user", "s_user_pass"}) do
    local s_res = ""
    while s_res == "" do
      s_res = tool.readline(string.format("Set up first admin config: %s [%s]> ", string.sub(v, 3), t_setup[v])) or "q"
      if s_res == "q" then io.write("\n"); os.exit(0) end
      s_res = (s_res == "") and t_setup[v] or s_res
      if (v == "s_domain") or (v == "s_client") then
        local t_res = tool.checkUserDomain(string.format("(domain)%s", s_res)) or {}
        s_res = t_res.s_object or ""
      elseif v == "s_user" then
        local t_res = tool.checkUserDomain(string.format("(user)%s@%s", s_res, t_setup.s_domain)) or {}
        s_res = t_res.s_object or ""
      elseif v == "s_redis_namespace" then
        s_res = string.gsub(s_res, "[^_A-Za-z0-9]", "")
      end
    end
    t_setup[v] = s_res
  end
  tool.t_config.s_redis_namespace = t_setup.s_redis_namespace
  t_setup.s_user_pass = tool.crypt(t_setup.s_user_pass, s_master_password, tool.ENCRYPT, "x")
  t_setup.s_client_pass = tool.crypt(t_setup.s_client_pass, s_master_password, tool.ENCRYPT, "x")
  r.q({
    {"SET", tool.getRedisKey("config_online"), 1},
    {"SET", tool.getRedisKey("config_client_pass", t_setup), t_setup.s_client_pass},
    {"SET", tool.getRedisKey("config_client_ip", t_setup), "127.0.0.1"},
    {"SADD", tool.getRedisKey("config_admin_flag_config_users", t_setup), t_setup.s_user},
    {"SADD", tool.getRedisKey("config_domain_list"), t_setup.s_domain},
    -- add localhost as well, so we can rely on having localhost always as a domain_list result
    {"SADD", tool.getRedisKey("config_domain_list"), "localhost"},
    {"SADD", tool.getRedisKey("domain_user_list", t_setup), t_setup.s_user},
    {"SADD", tool.getRedisKey("user_admin_flags", t_setup), tool.t_config.t_admin_flag.config},
    {"SET", tool.getRedisKey("user_pass", t_setup), t_setup.s_user_pass},
  })

-- For more options / details on config call: %s -hc
  local t_config = {Global = string.format(tool.t_config.s_global_config_template,
    s_master_password,
    s_redis_dsn,
    tool.t_config.s_redis_namespace
  ),
  User = string.format([[
user = "%s"
user_pass = "%s"
client = "%s"
client_pass = "%s"

]],
    t_setup.s_user,
    t_setup.s_user_pass,
    t_setup.s_client,
    t_setup.s_client_pass
  )}
  for s_k, s_v in pairs(t_config) do
    local b_config_written = false
    local s_config_file = (s_k == "Global") and tool.t_config.s_global_config or tool.t_config.s_user_config
    if not(tool.fileExists(s_config_file)) then
      local f_w = io.open(s_config_file, "w")
      if f_w then
        f_w:write(s_v)
        f_w:close()
        io.write(string.format("%s config written to: %s\n", s_k, s_config_file))
        b_config_written = true
      end
    end
    if not(b_config_written) then
      local s_config_file_substitute = string.format("%s/%s_%s.config.lua", (os.getenv("HOME") or "."), tool.t_config.s_redis_namespace, s_k)
      local f_w = io.open(s_config_file_substitute, "w")
      if f_w then
        f_w:write(s_v)
        f_w:close()
        io.write(string.format("%s config written to: %s\nTo use default setup, move file to: %s\n", s_k, s_config_file_substitute, s_config_file))
      else
        io.write(string.format("Cannot write config file: %s\nTo use default setup, place this config into it:\n%s\n", s_config_file, s_v))
      end
    end
  end
  if not(string.find(string.lower(tool.readline("Get a List of Top Level Domains for checks? [Y/n]> ")), " *n *")) then
    local s_fetch_tld_list = tool.getTldsAlphaByDomain()
    if s_fetch_tld_list then
      io.write(string.format("Put the file \"%s\" into your LUA_PATH (madm) directory.\n -> LUA_PATH=\"%s\"\n", s_fetch_tld_list, LUA_PATH))
    end
  end
  io.write(string.format("You can get an update with \"%s --fetch-tlds\"\n", tool.t_config.s_program_name))
  io.write(string.format("Install is completed start console (%s) to configure.\n", tool.t_config.s_program_name))
end

return _M

