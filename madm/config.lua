local _FILENAME = "madm/config.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20180925"

local _M = {}
local pairs, setmetatable = pairs, setmetatable
local os = {getenv = os.getenv}
local string = {format = string.format}
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

_M.t_admin_flag = {
  config = "config",
  global_domain = "global_domain",
  read_data = "read_data",
  read_only = "read_only",
  read_pass = "read_pass",
}

_M.connection_data_id = 1

_M.t_action = {
  get = true,
  set = true,
  new = true,
  mov = true,
  del = true,
}

_M.t_status_code = {unknown = 0, ok = 2.0, ok_auth = 2.1, error_threshold = 3.0, client = 4.0, client_auth = 4.1, client_auth_tmp = 4.2, server = 5.0, server_data_connection = 5.1, server_data_save = 5.2}
local t_reverse_status_code = {}
for k, v in pairs(_M.t_status_code) do t_reverse_status_code[v] = k; end
setmetatable(_M.t_status_code, {__index = t_reverse_status_code})

_M.s_global_config_template = [[
--LUA_PATH="MY_LUA_PATH/?.lua;;"
--LUA_CPATH="MY_LUA_CPATH/?.so;;"
DATA_MASTER_PASSWORD = [=[%s]=]
redis_dsn = "%s"
--redis_slave_dsn =
redis_namespace = "%s"
-- may be "SAVE" or "BGSAVE" - "set" action may return error
--redis_save = "SAVE"
--local log_config = {
--  ERROR = "stderr",
--  DELETED_OBJECT = "stdout",
--  WARN = "stdout",
--  INFO = "stdout",
--  ["%e"] = true, -- use environment LOG_*
--}
--log = require("flog").init(log_config)
]]

local nolog = function() end
_M.log = setmetatable({}, {__index = function() return nolog end})

_M.t_mailbox_type = {"maildir", "mbox"}

_M.t_module_internal = {
  client = {"madm.client", "essential part of madm package - required for all client requests"},
  client_shell = {"madm.client_shell", "required for client shell"},
  config = {"madm.config", "essential part of madm package - used for all default config settings"},
  help = {"madm.help", "gives information"},
  install = {"madm.install", "required for installation purpose only"},
  server = {"madm.server", "essential part of madm package - required on server"},
  socket_server = {"madm.socket_server", "required for madm_socket and dovecot_auth_socket"},
  tool = {"madm.tool", "essential part of madm package - provides initialization and functions"},
}
_M.t_module = {
  tool = {"tool", "madm requires: \"tool\" = {mkdir, fileExists, crypt, hash, getUsec, readline, addHistory} (https://gitlab.com/mdkmde/tool_lua/)\n HINT:  (\"tool\" contains some native c/gnu/linux functions it may not work on other systems)"},
  socket = {"socket", "redis / cgi require: \"socket\" from luasocket (https://github.com/diegonehab/luasocket)"},
  redis = {"redis", "madm requires: \"redis\" as data storage lib  (https://gitlab.com/mdkmde/redis_lua/)"},
  cache = {"cache", "madm requires: \"cache\" for better socket_server performance (https://gitlab.com/mdkmde/cache_lua/)"},
  cgi = {"cgi", "madm requires: \"cgi\" for remote/API requests (https://gitlab.com/mdkmde/cgi_lua/)"},
  json = {"dkjson", "madm requires: \"dkjson\" for remote/API requests (http://dkolf.de/src/dkjson-lua.fsl/home)"},
  ssl = {"ssl", "client SSL request requires: \"ssl\" from luasec (https://github.com/brunoos/luasec)"},
  flog = {"flog", "madm can use log.LEVEL(\"FORMAT_STRING\"[, ..]) (https://gitlab.com/mdkmde/log_lua/)"},
}

_M.t_permission_role = {user = 0, domain = 1, global_domain = 2, config = 3, internal = 4}

_M.s_program_name = "madm"

_M.s_redis_dsn = "redis://"
_M.t_redis_key = {
  -- {permission, value_type, default_value, delete_value, bool_value, redis_key}
  -- nil on bool_value => neither bool nor any other -> read_only
  internal_config_domain_list_inactive = {"internal", "table", {}, nil, false, {"CONFIG", "DOMAIN_LIST_INACTIVE"}},
  internal_domain_user_list_inactive = {"internal", "table", {}, nil, false, {"DOMAIN", "s_domain", "USER_LIST_INACTIVE"}},
  internal_user_autore_sent_to = {"internal", "table", {}, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "AUTORE_SENT_TO"}},
  internal_user_rid = {"internal", "number", 0, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "RID"}},
  internal_user_mailbox_limit_mail_sent = {"internal", "number", 0, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX_LIMIT_MAIL_SENT"}},


  config_admin_flag_config_users = {"config", "table", {}, nil, false, {"CONFIG", "CONFIG_USERS"}},
  config_admin_flag_global_domain_users = {"config", "table", {}, nil, false, {"CONFIG", "GLOBAL_DOMAIN_USERS"}},
  config_admin_flag_read_data_users = {"config", "table", {}, nil, false, {"CONFIG", "READ_DATA_USERS"}},
  config_admin_flag_read_only_users = {"config", "table", {}, nil, false, {"CONFIG", "READ_ONLY_USERS"}},
  config_admin_flag_read_pass_users = {"config", "table", {}, nil, false, {"CONFIG", "READ_PASS_USERS"}},
  config_debug_client_auth = {"config", "number", 0, "0", true, {"CONFIG", "DEBUG_CLIENT_AUTH"}},
  config_enable_cgi_request_method_get = {"config", "number", 0, "0", true, {"CONFIG", "ENABLE_CGI_REQUEST_METHOD_GET"}},
  config_mailbox_path = {"config", "string", nil, "", false, {"CONFIG", "MAILBOX_PATH"}},
  config_mailbox_type = {"config", "string", _M.t_mailbox_type[1], "", false, {"CONFIG", "MAILBOX_TYPE"}},
  config_mailbox_limit_max = {"config", "string", nil, "0", false, {"CONFIG", "MAILBOX_LIMIT_MAX"}},
  config_mailbox_limit_sender = {"config", "string", "postmaster", "", false, {"CONFIG", "MAILBOX_LIMIT_SENDER"}},
  config_mailbox_limit_subject = {"config", "string", "Your mailbox is FULL. (${SIZE}/${LIMIT}) - Mails will be deferred!", "", false, {"CONFIG", "MAILBOX_LIMIT_SUBJECT"}},
  config_mailbox_limit_subject_warn = {"config", "string", "WARN: Your mailbox gets full. (${SIZE}/${LIMIT})", "", false, {"CONFIG", "MAILBOX_LIMIT_SUBJECT_WARN"}},
  config_mailbox_limit_text = {"config", "string", nil, "", false, {"CONFIG", "MAILBOX_LIMIT_TEXT"}},
  config_mailbox_delete_command = {"config", "string", "rm -rf ${MAILBOX}", "", false, {"CONFIG", "MAILBOX_DELETE_COMMAND"}},
  config_mailbox_delete_path_min_length = {"config", "number", 8192, "0", false, {"CONFIG", "MAILBOX_DELETE_PATH_MIN_LENGTH"}},
  config_mailbox_move_command = {"config", "string", "mv -f ${MAILBOX} ${DESTINATION}", "", false, {"CONFIG", "MAILBOX_MOVE_COMMAND"}},
  config_mailbox_size_cache_interval = {"config", "string", "1h", "0", false, {"CONFIG", "MAILBOX_SIZE_CACHE_INTERVAL"}},
  config_mailbox_size_check_command = {"config", "string", "du -bs ${MAILBOX} | cut -f1", "", false, {"CONFIG", "MAILBOX_SIZE_CHECK_COMMAND"}},
  config_online = {"config", "number", nil, "0", true, {"CONFIG", "ONLINE"}},
  config_password_min_length = {"config", "number", 8, "0", false, {"CONFIG", "PASSWORD_MIN_LENGTH"}},
  config_password_condition = {"config", "table", {}, nil, false, {"CONFIG", "PASSWORD_CONDITION"}},

  config_domain_list = {"global_domain", "number", nil, nil, nil, {"CONFIG", "DOMAIN_LIST"}},
  config_mailbox_limit = {"global_domain", "string", nil, "0", false, {"CONFIG", "MAILBOX_LIMIT"}},
  config_mailbox_limit_warn = {"global_domain", "string", nil, "0", false, {"CONFIG", "MAILBOX_LIMIT_WARN"}},

  config_client_list = {"config", "number", nil, nil, nil, {"CONFIG", "CLIENT_LIST"}},
  config_client_ip = {"config", "string", "", nil, false, {"CONFIG", "CLIENT", "s_client", "IP"}},
  config_client_pass = {"config", "string", nil, nil, false, {"CONFIG", "CLIENT", "s_client", "PASS"}},


  domain_mailbox_path = {"config", "string", nil, "", false, {"DOMAIN", "s_domain", "MAILBOX_PATH"}},
  domain_mailbox_type = {"config", "string", _M.t_mailbox_type[1], "", false, {"DOMAIN", "s_domain", "MAILBOX_TYPE"}},

  domain_mailbox_limit_max = {"global_domain", "string", true, "0", false, {"DOMAIN", "s_domain", "MAILBOX_LIMIT_MAX"}},

  domain_created = {"domain", "string", "", nil, nil, {"DOMAIN", "s_domain", "CREATED"}},
  domain_admin_users = {"domain", "table", {}, nil, false, {"DOMAIN", "s_domain", "ADMIN_USERS"}},
  domain_alias = {"domain", "string", "", "", false, {"DOMAIN", "s_domain", "ALIAS"}},
  domain_catchall = {"domain", "string", "", "", false, {"DOMAIN", "s_domain", "CATCHALL"}},
  domain_mailbox_limit = {"domain", "string", true, "0", false, {"DOMAIN", "s_domain", "MAILBOX_LIMIT"}},
  domain_mailbox_limit_warn = {"domain", "string", true, "0", false, {"DOMAIN", "s_domain", "MAILBOX_LIMIT_WARN"}},
  domain_user_list = {"domain", "number", nil, nil, nil, {"DOMAIN", "s_domain", "USER_LIST"}},


  user_admin_flags = {"config", "table", {}, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "ADMIN_FLAGS"}},
  user_mailbox_path = {"config", "string", nil, "", false, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX_PATH"}},
  user_mailbox_type = {"config", "string", _M.t_mailbox_type[1], "", false, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX_TYPE"}},
  user_restricted_clients = {"config", "table", {}, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "RESTRICTED_CLIENTS"}},

  user_admin_domains = {"global_domain", "table", {}, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "ADMIN_DOMAINS"}},
  user_internal_alias_from_list = {"global_domain", "table", {}, nil, nil, {"DOMAIN", "s_domain", "USER", "s_user", "ALIAS_FROM"}},

  user_created = {"domain", "string", "", nil, nil, {"DOMAIN", "s_domain", "USER", "s_user", "CREATED"}},
  user_last_access = {"domain", "string", "", nil, nil, {"DOMAIN", "s_domain", "USER", "s_user", "LAST_ACCESS"}},
  user_mailbox_limit = {"domain", "string", true, "0", false, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX_LIMIT"}},

  user_aliases = {"user", "table", {}, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "ALIASES"}},
  user_mailbox = {"user", "number", nil, "0", true, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX"}},
  user_mailbox_limit_warn = {"user", "string", true, "0", false, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX_LIMIT_WARN"}},
  user_mailbox_size = {"user", "string", true, nil, nil, {"DOMAIN", "s_domain", "USER", "s_user", "MAILBOX_SIZE"}},
  user_pass = {"user", "string", nil, nil, false, {"DOMAIN", "s_domain", "USER", "s_user", "PASS"}},

  user_autore_start = {"user", "string", nil, "", false, {"DOMAIN", "s_domain", "USER", "s_user", "AUTORE_START"}},
  user_autore_end = {"user", "string", nil, "", false, {"DOMAIN", "s_domain", "USER", "s_user", "AUTORE_END"}},
  user_autore_subject = {"user", "string", nil, "", false, {"DOMAIN", "s_domain", "USER", "s_user", "AUTORE_SUBJECT"}},
  user_autore_text = {"user", "string", nil, "", false, {"DOMAIN", "s_domain", "USER", "s_user", "AUTORE_TEXT"}},
  user_autore_reply_as = {"user", "string", "", "", false, {"DOMAIN", "s_domain", "USER", "s_user", "AUTORE_REPLY_AS"}},
}
_M.s_redis_namespace =  "madm"

_M.response_value_separator = ", "

_M.rhash_algo = "sha1"
_M.rhash_armor = "x"

_M.s_shell_list_option = "-a10o"

_M.n_socket_server_backlog = 256
_M.n_socket_server_load_cache_size = 10000
_M.n_socket_server_redis_cache_size = 10000
_M.n_socket_server_redis_cache_ttl = 10
_M.n_socket_server_redis_cache_load_err_ttl = 300

_M.s_user_config = string.format("%s/.madm.conf.lua", os.getenv("HOME"))

return _M

