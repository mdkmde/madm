local _FILENAME = "madm/client_shell.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20180117"

local _M = {}
local getmetatable, pairs, setmetatable, tonumber, tostring, type = getmetatable, pairs, setmetatable, tonumber, tostring, type
local io = {write = io.write}
local os = {exit = os.exit}
local string = {find = string.find, format = string.format, gsub = string.gsub, lower = string.lower, sub = string.sub}
local table = {concat = table.concat, sort = table.sort}
local tool = require("madm.tool")
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

local t_complete = {s_object_type = "config", t_command = {}, t_connection_data = {}}
local doCompletion = function(is_text, is_line, in_start, in_end)
  local t_result = {}
  local addTable = function(it)
    for _, s_v in pairs(it or {}) do
      if string.find(s_v, string.format("^%s", is_text)) then
        t_result[#t_result + 1] = s_v
      end
    end
  end
  if in_start <= 2 then
    addTable(t_complete.attribute)
    addTable(t_complete.t_command)
  elseif string.find(is_line, "^ *l ") then
    if (in_start <= 4) then
      addTable({"-ax", "-a", "-o", "-c", "-co"})
    end
    if not(string.find(is_line, "^ *l  *%-")) or string.find(is_line, "^ *l  *%-[^ ]*o") then
      if t_complete.s_object_level == "domain" then
        addTable(t_complete.user_list_active)
        addTable(t_complete.user_list_inactive)
      elseif t_complete.s_object_level == "config" then
        addTable(t_complete.domain_list_active)
        addTable(t_complete.domain_list_inactive)
      end
    end
    if t_complete.s_object_level == "config" and (not(string.find(is_line, "^ *l  *%-")) or string.find(is_line, "^ *l  *%-[^ ]*c")) then
      addTable(t_complete.client_list)
    end
  elseif string.find(is_line, "@", in_start - 2) == (in_start - 1) then
    addTable(t_complete.domain_list_active)
    addTable(t_complete.domain_list_inactive)
  elseif string.find(is_line, "^ *c ") and (in_start <= 4) then
    if not(t_complete.s_object_level == "config") then
      addTable(t_complete.user_list_active)
      addTable(t_complete.user_list_inactive)
    end
    addTable(t_complete.domain_list_active)
    addTable(t_complete.domain_list_inactive)
    addTable(t_complete.client_list)
  elseif string.find(is_line, "^ *admin_flags *= *") then
    addTable(tool.t_config.t_admin_flag)
  elseif string.find(is_line, "^ *connection_data *= *[^ ]*$") then
    addTable(t_complete.t_connection_data)
  elseif string.find(is_line, "^ *mailbox_type *= *[^ ]*$") then
    addTable(t_mailbox_type)
  elseif string.find(is_line, "^ *[nd]e[wl] +[^ ]*$") then
    return {""}
  elseif string.find(is_line, "^ *mov +[^ ]+ +[^ ]+ +[^ ]*$") then
    return {"set_alias"}
  elseif string.find(is_line, "^ *mov +.*") then
    return {""}
  elseif 3 < in_start and not(string.find(is_line, "=")) then
    addTable({"="})
    addTable(t_complete.attribute)
  end
  if 0 < #t_result then
    return t_result
  end
  if not(string.find(is_line, "^ *mailbox_path *= *[^ ]*$")) then
    return {""}
  end
end
tool.setCompletion(doCompletion)

local client = tool.require("madm.client")
local doRequest = function(it_request)
  it_request.rid = nil
  return client.doRequest(it_request)
end

local s_connection_data_prompt
_M.run = function()
  if not(tool.t_config.s_user) or not(tool.t_config.s_user_pass) or not(tool.t_config.client) or not(tool.t_config.client_pass) then
    io.write("ERROR = \"Missing config data.\"\n")
    os.exit(1)
  end
  local t_result = {}
  local t_server_result = {}
  local t_request = {
    object = "",
    s_object_level = "config",
    action = "get",
    attributes = {"online"},
  }
  local t_server_result = doRequest(t_request)
  if t_server_result.status == 0 then
    io.write(string.format("ERROR: %q", t_server_result.error.message))
    os.exit(1)
  end

  local t_attribute = {config = {}, domain = {"active"}, user = {"active", "autore_active", "recipient_data", "mailbox_data"}}
  for k, _ in pairs(t_attribute) do
    for s_key, _ in pairs(tool.t_config.t_redis_key) do
      string.gsub(s_key, string.format("^%s_(.*)", k), function(is_attr)
        if not(string.find(is_attr, "^client_")) and not(string.find(is_attr, "_list$")) then
          t_attribute[k][#t_attribute[k] + 1] = string.format("%s", is_attr)
        end
      end)
    end
    table.sort(t_attribute[k])
  end
  t_attribute.client = {"active", "pass", "ip"}
  local getAttribute = function()
    if string.find(t_request.object, "@$") then
      t_complete.attribute = t_attribute.client
      return t_attribute.client
    end
    t_complete.attribute = t_attribute[t_request.s_object_level]
    return t_attribute[t_request.s_object_level]
  end

  local t_command = {
    q = function() io.write("\n"); os.exit(0); end,
    l = function(is_arg)
      local filterResult = function(it_data, is_filter)
        is_filter = string.gsub(is_filter, "%*", "")
        if is_filter == "" then
          return it_data
        end
        local t_out_data = {}
        for n_k = 1, #it_data do
          if string.find(it_data[n_k], is_filter) then
            t_out_data[#t_out_data + 1] = it_data[n_k]
          end
        end
        return t_out_data
      end

      local lAttributeValue = function(is_filter)
        local t_out = {}
        t_request.action = "get"
        t_request.attributes = {}
        local getStringResult = function(i_result)
          local s_data
          if type(i_result) == "number" then
            s_data = string.format("%d", i_result)
          elseif type(i_result) == "table" then
            s_data = string.format("%s", table.concat(i_result, " "))
          else
            s_data = string.format("%s", i_result or "")
          end
          return s_data
        end
        if string.find(t_request.object, "@$") then
          t_request.attributes = {"active", "ip"}
          t_server_result = doRequest(t_request)
          t_out[#t_out + 1] = string.format("A active = %s", t_server_result.data.active or "")
          t_out[#t_out + 1] = string.format("A ip = %s", t_server_result.data.ip or "")
          if tonumber(t_server_result.data.active) == 1 then
            t_request.attributes = {"pass"}
            t_server_result = doRequest(t_request)
            t_out[#t_out + 1] = string.format("A pass = %s", t_server_result.error and "?" or (t_server_result.data and t_server_result.data.pass or ""))
          else
            t_out[#t_out + 1] = "pass = \"\""
          end
          t_server_result = {status = 1}
        else
          for _, s_attr in pairs(filterResult(getAttribute(), is_filter)) do
            local t_special_permission = {pass = true, mailbox_data = true, recipient_data = true, admin_flags = true, mailbox_path = true, mailbox_type = true, restricted_clients = true}
            if t_special_permission[s_attr] then
              local t_attributes = t_request.attributes
              t_request.attributes = {s_attr}
              t_server_result = doRequest(t_request)
              if s_attr == "pass" then
                t_out[#t_out + 1] = string.format("A pass = %s", t_server_result.error and "?" or (t_server_result.data and t_server_result.data.pass or ""))
              elseif t_server_result.data then
                local s_data = getStringResult(t_server_result.data[s_attr])
                if not(s_data == "") then
                  t_out[#t_out + 1] = string.format("A %s = %s", s_attr, s_data)
                end
              end
              t_request.attributes = t_attributes
            elseif not(string.find(s_attr, "^client_")) then
              t_request.attributes[#t_request.attributes + 1] = s_attr
            end
          end
          if 0 < #t_request.attributes then
            t_server_result = doRequest(t_request)
            if t_server_result.data then
              for s_k, o_result in pairs(t_server_result.data) do
                local s_data = getStringResult(o_result)
                if not(s_data == "") then
                  t_out[#t_out + 1] = string.format("A %s = %s", s_k, s_data)
                end
              end
            end
          end
        end
        table.sort(t_out)
        if #t_result == 0 then
          t_result = t_out
        else
          for n_k = 1, #t_out do
            t_result[#t_result + 1] = t_out[n_k]
          end
        end
      end

      local lAttribute = function(is_filter)
        local t_attribute = filterResult(getAttribute(), is_filter)
        if 0 < #t_attribute then
          t_result[#t_result + 1] = string.format("A %s", table.concat(t_attribute, "\nA "))
        end
      end

      local lClient = function(is_filter)
        t_request.action = "get"
        local s_object = t_request.object
        t_request.object = ""
        t_request.attributes = {"client_list"}
        t_server_result = doRequest(t_request)
        t_request.object = s_object
        t_request.attributes = {}
        if t_server_result.data and t_server_result.data.client_list then
          t_complete.client_list = t_server_result.data.client_list
          local t_result_data = filterResult(t_server_result.data.client_list, is_filter)
          if (0 < #t_result_data) then
            t_result[#t_result + 1] = string.format("C %s", table.concat(t_result_data, "\nC "))
          end
        end
      end

      local lObject = function(is_filter, in_sum, is_show_type)
        t_request.action = "get"
        local t_type = {"user_list_inactive", "user_list_active", "o", "O", "<", ">"}
        if t_request.s_object_level == "config" then
          t_type[1] = "domain_list_inactive"
          t_type[2] = "domain_list_active"
        end
        t_request.attributes = {t_type[1], t_type[2]}
        t_server_result = doRequest(t_request)
        if not(t_server_result.data) and (t_request.s_object_level == "config") then
          local s_object = t_request.object
          t_request.object = tool.t_config.s_user
          t_request.attributes = {"admin_domains"}
          t_server_result = doRequest(t_request)
          t_request.object = s_object
          if t_server_result.data then
            t_server_result.data.domain_list_active = t_server_result.data.admin_domains
          end
        end
        if t_server_result.data then
          for i = 1, 2 do
            t_server_result.data[t_type[i]] = t_server_result.data[t_type[i]] or {}
            t_complete[t_type[i]] = t_server_result.data[t_type[i]]
            local t_result_data = filterResult(t_server_result.data[t_type[i]], is_filter)
            if 0 < #t_result_data then
              if (0 <= in_sum) and (in_sum <= #t_result_data) then
                t_result_data[#t_result_data + 1] = string.format("-> %d %s%s", #t_result_data, string.gsub(t_type[i], "^([^_]+)_[^_]+_([^_]+)", "%2 %1"), (#t_result_data == 1) and "" or "s")
              end
              t_result[#t_result + 1] = string.find(is_show_type, t_type[i + 4]) and string.format("%s %s", t_type[i + 2], table.concat(t_result_data, string.format("\n%s ", t_type[i + 2]))) or ((in_sum == 0) and string.format("%s %s", t_type[i + 2], t_result_data[#t_result_data]) or nil)
            end
          end
        else
          t_server_result = {status = 1}
        end
      end

      if not(string.find(is_arg, "%-")) then
        local s_shell_list_option = string.find(is_arg, "%*") and string.gsub(tool.t_config.s_shell_list_option, "ax?", "ax") or tool.t_config.s_shell_list_option
        is_arg = string.format("%s %s", s_shell_list_option, is_arg)
      end
      local t_option = {}
      local s_filter = string.gsub(is_arg, " *%-([^ ]*) *", function(is_option)
        is_option = string.lower(is_option)
        while not(is_option == "") do
          if not(t_option["ax"]) and string.find(is_option, "^ax") then
            t_option[#t_option + 1] = lAttributeValue
            t_option["ax"] = true
          elseif not(t_option["a"]) and string.find(is_option, "^a") then
            t_option[#t_option + 1] = lAttribute
            t_option["a"] = true
          end
          if not(t_option["c"]) and string.find(is_option, "^c") then
            if (t_request.s_object_level == "config") and not(string.find(t_request.object, "@$")) then
              t_option[#t_option + 1] = lClient
              t_option["c"] = true
            end
          end
          if not(t_option["o"]) and string.find(is_option, "^[<>0-9]*o") then
            if not(t_request.s_object_level == "user") and not(string.find(t_request.object, "@$")) then
              t_option[#t_option + 1] = lObject
              t_option["o"] = true
              t_option["object_sum"] = tonumber((string.gsub(is_option, "^[^0-9o]*([0-9]*)[<>]*o[^<>0-9]*$", "%1"))) or -1
              t_option["object_type"] = string.gsub(is_option, "^[^<>o]*([<>]*)[0-9]*o[^<>0-9]*$", function(is_type)
                if not(t_option["object_sum"] == 0) and is_type == "" then
                  is_type = "<>"
                end
                return is_type
              end)
            end
          end
          is_option = string.sub(is_option, 2)
        end
        return ""
      end)
      for n_k = 1, #t_option do
        t_option[n_k](s_filter, t_option["object_sum"], t_option["object_type"])
      end
      if #t_result == 0 then
        t_result = {""}
      end
    end,
    c = function(is_arg)
      if is_arg == ".." then
        if t_request.s_object_level == "user" then
          is_arg = string.gsub(t_request.object, ".*@", "")
        else
          is_arg = ""
        end
      elseif is_arg == "-" then
        is_arg = t_request.s_last_object or ""
      end
      t_request.s_last_object = t_request.object
      t_request.object = is_arg
      if not(is_arg == "") then
        t_request.action = "get"
        t_request.attributes = {"active"}
        t_server_result = doRequest(t_request)
      end
      if (is_arg == "") or (t_server_result.data and t_server_result.data.active) or (string.find(is_arg, "@") == #is_arg) then
        if is_arg == "" then
          t_request.s_object_level = "config"
        elseif string.find(is_arg, "@") then
          t_request.s_object_level = "user"
        else
          t_request.s_object_level = "domain"
        end
        t_complete.s_object_level = t_request.s_object_level
        if not(t_server_result.data and (t_server_result.data.active == 1)) and string.find(is_arg, "@$") then
          t_result = {"WARN: object unknown - set pass to create new client."}
          getAttribute()
        end
        return
      elseif string.find(is_arg, "@") then
        t_request.action = "get"
        t_request.attributes = {"mailbox"}
        t_server_result = doRequest(t_request)
        if t_server_result.data then
          t_request.s_object_level = "user"
          t_complete.s_object_level = t_request.s_object_level
          return
        end
      else
        t_request.action = "get"
        t_request.attributes = {"alias"}
        t_server_result = doRequest(t_request)
        if t_server_result.data then
          t_request.s_object_level = "domain"
          t_complete.s_object_level = t_request.s_object_level
          return
        end
      end
      t_request.object = t_request.s_last_object
      t_result = {"ERROR: object unknown."}
    end,
    new = function(is_arg)
      local s_cur_object = t_request.object
      t_request.action = "new"
      t_request.object = is_arg
      t_server_result = doRequest(t_request)
      t_request.object = s_cur_object
    end,
    del = function(is_arg)
      local s_cur_object = t_request.object
      t_request.action = "del"
      t_request.object = is_arg
      t_server_result = doRequest(t_request)
      if (t_server_result.status == 1) and (t_request.object == s_cur_object) then
        if t_request.s_object_level == "user" then
          t_request.s_object_level = "domain"
          t_request.object = string.gsub(s_cur_object, ".*@", "")
        else
          t_request.s_object_level = "config"
          t_request.object = ""
        end
      else
        t_request.object = s_cur_object
      end
    end,
    mov = function(is_arg)
      local s_cur_object = t_request.object
      t_request.action = "mov"
      t_request.object = string.gsub(is_arg, " .*$", "")
      t_request.attributes = {}
      string.gsub(string.gsub(is_arg, "^[^ ]* ", ""), "([^ ]+) *", function(is_arg) t_request.attributes[#t_request.attributes + 1] = is_arg; end)
      t_server_result = doRequest(t_request)
      if (t_server_result.status == 1) and (t_request.object == s_cur_object) then
        t_request.object = t_request.attributes[1]
      else
        t_request.object = s_cur_object
      end
    end,
    connection_data = function(is_arg, b_init)
      t_result = {}
      if string.find(is_arg, "^ *= *") then
        is_arg = string.gsub(is_arg, " *= *([^ ]+) *", "%1")
        is_arg = tonumber(is_arg) or not(is_arg == "") and is_arg or 1
        tool.t_config.connection_data_id = tool.t_config.t_server_url[is_arg] and is_arg or 1
        s_connection_data_prompt = string.format("[%0.9s]", (tool.t_config.connection_data_id == 1) and "" or tostring(tool.t_config.connection_data_id))
        t_request.ruser = nil
        tool.updateConnectionData()
      else
        for s_k, s_v in pairs(tool.t_config.t_server_url) do
          s_k = tostring(s_k)
          if b_init then
            t_complete.t_connection_data[#t_complete.t_connection_data + 1] = (s_k == "1") and "" or s_k
          else
            t_result[#t_result + 1] = string.format("%25s%s(%s)", (s_k == "1") and "" or s_k, (tostring(tool.t_config.connection_data_id) == s_k) and "*" or " ", (type(s_v) == "string") and s_v or "localhost")
          end
        end
        if b_init then
          if not(tool.t_config.t_server_url[1]) then
            t_complete.t_connection_data[#t_complete.t_connection_data + 1] = ""
            tool.t_config.t_server_url[1] = true
            table.sort(tool.t_config.t_server_url)
            table.sort(t_complete.t_connection_data)
          end
          if #t_complete.t_connection_data < 2 then
            s_connection_data_prompt = nil
          else
            s_connection_data_prompt = string.format("[%0.9s]", (tool.t_config.connection_data_id == 1) and "" or tostring(tool.t_config.connection_data_id))
          end
        end
      end
    end,
    h = function()
      t_result = tool.require("madm.help").t_shell_help
    end,
  }

  t_command.connection_data("", true)
  if not(s_connection_data_prompt) then
    s_connection_data_prompt = ""
    t_command.connection_data = nil
    t_complete.t_connection_data = {""}
  end
  for s_k, _ in pairs(t_command) do
    t_complete.t_command[#t_complete.t_command + 1] = s_k
  end
  t_server_result = {status = 1}
  while true do
    if t_server_result.status == 0 then
      t_result = {string.format("ERROR: %q", t_server_result.error.message)}
    end
    t_request.attributes = {}
    t_request.values = nil
    if 0 < #t_result then
      io.write(table.concat(t_result, "\n"))
      io.write("\n")
    else
      -- prefetch readline complete
      t_command.l("-aco")
      t_request.attributes = {}
    end
    t_result = {}
    t_request.s_request = tool.readline(string.format("%s%s:%s> ", tool.t_config.s_program_name, s_connection_data_prompt, t_request.object)) or "q"
    if not(string.sub(t_request.s_request or "", 1, 1) == " ") and not(string.find(t_request.s_request, "^[lhq] *[aoc]*x?$")) then
      tool.addHistory(t_request.s_request)
    end
    string.gsub(t_request.s_request, "^ *([^ =]+) *(.*)", function(is_cmd, is_arg)
      is_arg = string.gsub(is_arg, " *$", "")
      if t_command[is_cmd] then
        t_server_result = {status = 1}
        t_command[is_cmd](is_arg)
      elseif string.find(is_cmd, "%*") then
        t_command.l(string.format("%s", is_cmd))
      else
        local s_attribute = string.format("|%s|", table.concat(getAttribute(), "|"))
        if not(string.find(s_attribute, string.format("|%s|", is_cmd))) then
          t_server_result = {status = 0, error = {message = "command or attribute not found. Use \"h\" for help."}}
        else
          t_result = {}
          if not(string.find(is_arg, "=")) then
            t_request.action = "get"
            t_request.attributes = {is_cmd}
            string.gsub(is_arg, " *([^ ]+)", function(is_attr)
              t_request.attributes[#t_request.attributes + 1] = is_attr
            end)
          else
            string.gsub(is_arg, " *= *(.*)", function(is_value)
              t_request.action = "set"
              t_request.attributes = {is_cmd}
              for s_key, t_key in pairs(tool.t_config.t_redis_key) do
                if (s_key == string.format("%s_%s", t_request.s_object_level, is_cmd)) and (t_key[2] == "table") then
                  local t_value = {}
                  local s_separator = " "
                  if string.find(is_value, "^[^=]=") then
                    s_separator = string.sub(is_value, 1,1)
                    is_value = string.gsub(is_value, string.format("%s= *", s_separator), "")
                  end
                  string.gsub(string.format("%s%s", is_value, s_separator), string.format("([^%s]*)%s+", s_separator, s_separator), function(is_arg) t_value[#t_value + 1] = is_arg; end)
                  is_value = t_value
                end
              end
              t_request.values = {is_value}
            end)
          end
          t_server_result = doRequest(t_request)
          t_server_result.data = t_server_result.data or {}
          for s_k, t_result_data in pairs(t_server_result.data) do
            if type(t_result_data) == "table" then
              if (#t_result < 1) then
                t_result = t_result_data
                setmetatable(t_result, getmetatable(t_result_data))
              else
                t_result[#t_result + 1] = string.format("%s = %s", s_k, table.concat(t_result_data, " "))
              end
            else
              t_result[#t_result + 1] = string.format("%s = %s", s_k, tostring(t_result_data))
            end
          end
          if (t_request.action == "get") and (#t_result == 1) and string.find(t_result[1], t_request.attributes[1]) then
            t_result[1] = string.gsub(t_result[1] or "", ".* = ", "")
          elseif #t_result == 0 then
            t_result = (t_request.action == "get") and {""} or {"OK."}
          end
          t_server_result = t_server_result or {status = 0, error = {message = "No result"}}
        end
      end
    end)
  end
end

return _M

