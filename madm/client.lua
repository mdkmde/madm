local _FILENAME = "madm/client.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170830"

local _M = {}
local pairs, setmetatable, tonumber, type = pairs, setmetatable, tonumber, type
local io = {write = io.write}
local math = {floor = math.floor}
local os = {date = os.date, exit = os.exit}
local string = {format = string.format, gsub = string.gsub}
local table = {concat = table.concat}
local tool = require("madm.tool")
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

_M.doRequest = function(it_request)
  it_request.ruser = it_request.ruser or tool.t_config.s_user
  it_request.rclient = it_request.rclient or tool.t_config.client
  it_request.rid = it_request.rid or tool.t_config.rid or string.format("%s%04d", string.gsub(os.date("!%Y%j%T"), "([0-2][0-9]):([0-5][0-9]):([0-5][0-9])$", function(in_h, in_m, in_s) return string.format("%05d", math.floor((in_h * 60 * 60) + (in_m * 60) + in_s)); end), math.floor(tool.getUsec() / 100))
  if not(type(tool.t_config.s_server_url) == "string") then
    local t_result
    if not(tool.connectRedis()) then
      return {status = 0, error = {message = "No redis connection"}}
    end
    if tool.redis then
      local server = tool.require("madm.server")
      t_result = server.doRequest(it_request)
      tool.closeRedis()
    end
    return t_result or {status = 0, error = {message = "No result"}}
  end
  local cgi = tool.require("cgi")
  local json = tool.require("json")
  it_request.rhash_algo = tool.t_config.rhash_algo
  it_request.rhash_armor = tool.t_config.rhash_armor
  local s_request = json.encode(it_request)
  tool.log.INFO("doClientRequest: START Request: %s", s_request)
  local s_pass = string.format("%s%s", tool.crypt(tool.t_config.client_pass, tool.t_config.DATA_MASTER_PASSWORD, tool.DECRYPT, "x"), tool.crypt(tool.t_config.s_user_pass, tool.t_config.DATA_MASTER_PASSWORD, tool.DECRYPT, "x"))
  local s_hash = tool.hash(s_request, it_request.rhash_algo, s_pass, it_request.rhash_armor)
  local t_server = {s_post_request = string.format("request=%s&hash=%s", cgi.urlEncode(s_request), cgi.urlEncode(s_hash))}
  string.gsub(tool.t_config.s_server_url, "http(s?)://([^:/]*):?([^/]*)(.*)", function(is_https, is_server_name, is_server_port, is_server_path)
    t_server.s_name = is_server_name
    t_server.n_port = tonumber(is_server_port) or ((is_https == "s") and 443 or 80)
    t_server.s_host = (t_server.n_port == 80) and t_server.s_name or string.format("%s:%d", t_server.s_name, t_server.n_port)
    t_server.s_path = is_server_path
    t_server.b_ssl = (is_https == "s")
  end)
  t_server.t_request = {
    string.format("POST %s HTTP/1.0", t_server.s_path),
    string.format("Host: %s", t_server.s_host),
    "Content-Type: application/x-www-form-urlencoded",
    string.format("Content-Length: %s", #t_server.s_post_request),
    "",
    t_server.s_post_request,
  }
  local tcp = tool.require("socket").tcp()
  if not(tcp) then
    io.write("ERROR = \"Cannot create socket\"\n")
    os.exit(1)
  end
  if not(tcp:connect(t_server.s_name, t_server.n_port)) then
    io.write("ERROR = \"Socket connect failed\"\n")
    os.exit(1)
  end
  if t_server.b_ssl then
    tcp = tool.require("ssl").wrap(tcp, {mode = "client", protocol = "tlsv1"})
    tcp:dohandshake()
  end
  tcp:send(table.concat(t_server.t_request, "\r\n"))
  while not(tcp:receive("*l") == "") do end
  local s_json_result = tcp:receive("*a")
  tool.log.INFO("doClientRequest: END Request: %s", string.gsub(s_json_result, "pass\": \"[^ ]* ", "pass\": \"***\" "))
  tcp:close()
  return json.decode(s_json_result) or {status = 0, error = {message = "No result"}}
end

_M.formatResult = function(it_result, is_response_value_separator)
  if it_result.status == 1 then
    if tool.t_config.result then
      local t_result = {}
      for s_k, s_v in pairs(tool.t_config.result) do
        t_result[s_k] = string.gsub(s_v, "%${([^}]+)}", it_result.data)
        tool.log.DEBUG("result: \"%s\" = \"%s\"", s_k, t_result[s_k])
      end
      it_result.data = t_result
    end
    local t_result = {}
    for s_k, s_v in pairs(it_result.data) do
      if type(s_v) == "table" then
        s_v = table.concat(s_v, is_response_value_separator or tool.t_config.response_value_separator)
      end
      t_result[#t_result + 1] = string.format("%s = %q\n", s_k, s_v)
    end
    return t_result
  else
    return {string.format("ERROR = %q\n", it_result.error.message)}
  end
end

return _M

