local _FILENAME = "madm/tool.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170830"

local _M = {}
local _G, package = _G, package
local loadfile, pairs, pcall, require, setmetatable, tonumber, type = loadfile, pairs, pcall, require, setmetatable, tonumber, type
local io = {open = io.open, popen = io.popen, write = io.write}
local math = {floor = math.floor, random = math.random, randomseed = math.randomseed}
local os = {date = os.date, exit = os.exit}
local string = {char = string.char, find = string.find, format = string.format, gsub = string.gsub, lower = string.lower, sub = string.sub}
local table = {concat = table.concat, unpack = table.unpack}
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

if not(pcall(require, "madm.config")) then
  io.write(string.format("ERROR = \"madm\" requires: madm.config - essential part of madm package. \"madm/\" folder has to be available in:\nLUA_PATH=%s\n", package.path))
  os.exit(1)
end
_M.t_config = require("madm.config")
_M.require = function(is_module, is_warn)
  local t_module_table = _M.t_config["t_module"][is_module]
  local s_module, n_replace = string.gsub(is_module, "[^.]*%.", "")
  if (n_replace == 1) then
    t_module_table = _M.t_config["t_module_internal"][s_module]
  end
  if not(pcall(require, t_module_table and t_module_table[1] or "")) then
    io.write(string.format("%s = %q\n", is_warn or "ERROR", t_module_table and string.format("Module %s - %s", is_module, t_module_table[2]) or string.format("module is unknown: \"%s\"", is_module)))
    if not(is_warn) then
      os.exit(1)
    end
    return
  end
  return require(t_module_table[1])
end
local tool

_M.setCompletion = function(i_doCompletion)
  tool.doCompletion = i_doCompletion
end

_M.getConfFileData = function(is_config_file)
  if not(type(is_config_file) == "string") then
    return {}
  end
  local t_data = {}
  local data = loadfile(is_config_file, "bt", setmetatable(t_data, {__index = _G}))
  if not(pcall(data)) then
    return {}
  end
  setmetatable(t_data, nil)
  return t_data
end

local checkDomain = function(is_domain)
-- rfc 103[45] / (http://www.iana.org/domains/root/db/ | http://data.iana.org/TLD/tlds-alpha-by-domain.txt)
-- localhost
  local VALID_DOMAIN_PATTERN = "^[a-z0-9][-a-z0-9.]*[a-z0-9]%.[a-z][-a-z]+"
  local VALID_SINGLE_CHAR_DOMAIN_PATTERN = "^[a-z0-9]%.[a-z][-a-z]+"
  local INVALID_DOMAIN_PATTERN = "%.%."
  local MAX_DOMAIN_LEN = 253
  is_domain = string.lower(is_domain)
  if is_domain == "localhost" then
    return is_domain
  end
  if _M.t_config.t_tlds_alpha_by_domain then
    local b_success = _M.t_config.t_tlds_alpha_by_domain[string.gsub(is_domain, ".*%.", "")]
    _M.log.DEBUG("checkDomain: found \"%s\"%s in tlds-alpha-by-domain list.", is_domain, b_success and "" or " not")
    if not(b_success) then
      return
    end
  end
  if (not(string.find(is_domain, VALID_DOMAIN_PATTERN)) and not(string.find(is_domain, VALID_SINGLE_CHAR_DOMAIN_PATTERN))) or string.find(is_domain, INVALID_DOMAIN_PATTERN) or (MAX_DOMAIN_LEN < #is_domain) then
    return
  end
  return is_domain
end

local checkUser = function(is_user)
-- rfc 532[12] http://en.wikipedia.org/wiki/Email_address
  local VALID_USER_PATTERN = "^[-%%a-zA-Z0-9!#$&'*+/=?^_`{|}~][-%%a-zA-Z0-9!#$&'*+/=?^_`{|}~.]*[-%%a-zA-Z0-9!#$&'*+/=?^_`{|}~]$"
  local VALID_SINGLE_CHAR_USER_PATTERN = "^[-%%a-zA-Z0-9!#$&'*+/=?^_`{|}~]$"
  local INVALID_USER_PATTERN = "%.%."
  local MAX_USER_LEN = 64
  if (not(string.find(is_user, VALID_USER_PATTERN)) and not(string.find(is_user, VALID_SINGLE_CHAR_USER_PATTERN))) or string.find(is_user, INVALID_USER_PATTERN) or (MAX_USER_LEN < #is_user) then
    return
  end
  return is_user
end

_M.checkUserDomain = function(is_object)
  local MAX_DOMAIN_USER_LEN = 254
  if not(is_object) then
    return
  end
  local s_default_type_cast = "s_user_domain"
  local t_object_type_cast = {u = "s_user_domain", d = "s_domain", c = "s_client"}
  local t_valid = {s_type = "config"}
  is_object = string.gsub(is_object, "^%(([^)]*)%)", function(is_cast)
    local lc_cast = string.lower(string.sub(is_cast, 1, 1))
    t_valid.s_cast = t_object_type_cast[lc_cast] and lc_cast or "X"
    if not(t_valid.s_cast) then
      _M.log.WARN("checkUserDomain: cast: \"%s\" is not valid.", is_cast)
    end
    return ""
  end);
  setmetatable(t_valid, {__index = function(t, is_key) return is_key == "s_object" and t_valid[t_valid.s_cast and t_object_type_cast[t_valid.s_cast] or s_default_type_cast] or nil end})
  if t_valid.s_cast and (t_valid.s_cast == "c") and not(string.find(is_object, "@", -1)) then
    is_object = string.format("%s@", is_object)
  end
  string.gsub(is_object, "^([^@]-)@?([^@]*)$", function(is_user, is_domain)
    if _M.log.DEBUG then
      _M.log.DEBUG("checkUserDomain: got: %s%s", (is_user == "") and is_user or string.format("%s@", is_user), is_domain)
    end
    t_valid.s_user = is_user
    t_valid.s_domain = is_domain
  end)
  t_valid.s_user = checkUser(t_valid.s_user or "")
  if t_valid.s_user and (t_valid.s_domain == "") then
    s_default_type_cast = "s_client"
    t_valid.s_client = t_valid.s_user
    t_valid.s_domain = nil
    t_valid.s_user = nil
    return t_valid
  end
  t_valid.s_domain = checkDomain(t_valid.s_domain or "")
  if t_valid.s_domain then
    t_valid.s_type = "domain"
    if t_valid.s_user then
      t_valid.s_type = t_valid.s_cast and t_valid.s_cast == "d" and "domain" or "user"
      t_valid.s_user_domain = string.format("%s@%s", t_valid.s_user, t_valid.s_domain)
      if (MAX_DOMAIN_USER_LEN < #t_valid.s_user_domain) then
        return
      end
    else
      s_default_type_cast = "s_domain"
    end
  end
  return t_valid
end

_M.updateConnectionData = function()
  _M.t_config.s_user = (type(_M.t_config.t_user_config.user) == "table") and (_M.t_config.t_user_config.user[_M.t_config.connection_data_id] or _M.t_config.t_user_config.user[1]) or _M.t_config.t_user_config.user
  _M.t_config.s_user_pass = (type(_M.t_config.t_user_config.user_pass) == "table") and (_M.t_config.t_user_config.user_pass[_M.t_config.connection_data_id] or _M.t_config.t_user_config.user_pass[1]) or _M.t_config.t_user_config.user_pass
  _M.t_config.s_server_url = _M.t_config.t_server_url[_M.t_config.connection_data_id] or _M.t_config.t_server_url[1]
end

_M.init = function(it_arg)
  local _save_package_path = package.path
  package.path = string.gsub(string.format(";%s;", package.path), ";[^;]*/madm/%?%.luac?;", ";")
  package.path = string.sub(string.gsub(package.path, ";%./%?%.luac?;", ";"), 2, -2)
  tool = _M.require("tool")
  package.path = _save_package_path
  setmetatable(_M, {__index = function(t, k) return tool[k]; end})

  _M.t_config = {t_default_config = _M.t_config}
  _M.t_config.response_value_separator = it_arg.s_response_value_separator
  _M.t_config.connection_data_id = it_arg.connection_data_id

  _M.t_config.t_global_config = _M.getConfFileData(it_arg.s_global_config)
  _M.t_config.t_user_config = _M.getConfFileData(it_arg.s_user_config or _M.t_config.t_default_config.s_user_config)

  _M.t_config.DATA_MASTER_PASSWORD = _M.t_config.t_global_config.DATA_MASTER_PASSWORD
  _M.t_config.s_redis_dsn = _M.t_config.t_global_config.redis_dsn
  _M.t_config.s_redis_slave_dsn = _M.t_config.t_global_config.redis_slave_dsn
  _M.t_config.s_redis_namespace = _M.t_config.t_global_config.redis_namespace
  _M.t_config.s_redis_save = _M.t_config.t_global_config.redis_save
  for k, v in pairs(_M.t_config.t_global_config.redis_key_addons or {}) do
    if not(_M.t_config.t_redis_key[k]) then
      _M.t_config.t_redis_key[k] = v
    end
  end
  _M.t_config.t_server_url = (type(_M.t_config.t_user_config.server_url) == "table") and _M.t_config.t_user_config.server_url or {_M.t_config.t_user_config.server_url}

  setmetatable(_M.t_config.t_global_config, {__index = _M.t_config.t_default_config})
  setmetatable(_M.t_config.t_user_config, {__index = _M.t_config.t_global_config})
  setmetatable(_M.t_config, {__index = _M.t_config.t_user_config})

  _M.log = _M.t_config.log
  _M.cgi_log = _M.t_config.cgi_log or _M.t_config.log
  _M.redis_log = _M.t_config.redis_log or _M.t_config.log

  _M.redis = {}
  _M.redis_ro = {}

  local b_success
  b_success, _M.t_config.t_tlds_alpha_by_domain = pcall(require, "madm.tlds-alpha-by-domain")
  if not(b_success) then
    b_success, _M.t_config.t_tlds_alpha_by_domain = pcall(require, "tlds-alpha-by-domain")
    if not(b_success) then
      _M.t_config.t_tlds_alpha_by_domain = nil
      _M.log.WARN("Cannot load \"tlds-alpha-by-domain\", get the file with \"%s --fetch-tlds\" and put it into LUA_PATH (madm) directory.", _M.t_config.s_program_name)
    end
  end
  _M.updateConnectionData()
  local t_request = {
    action = it_arg[1],
    object = it_arg[2] or "",
    attributes = it_arg[3],
    values = it_arg[4],
  }
  if _M.log.DEBUG then
    _M.log.DEBUG("got action: \"%s\"", t_request.action or "")
    _M.log.DEBUG("got object: \"%s\"", t_request.object or "")
    _M.log.DEBUG("got attributes: \"%s\"", t_request.attributes or "")
    _M.log.DEBUG("got values: \"%s\"", t_request.values or "")
  end
  return t_request
end

_M.generatePassword = function(in_len)
  in_len = tonumber(in_len) or _M.t_config.t_redis_key["config_password_min_length"][3]
  in_len = (_M.t_config.t_redis_key["config_password_min_length"][3] <= in_len) and in_len or _M.t_config.t_redis_key["config_password_min_length"][3]
  local t_rand = {}
  math.randomseed(tonumber(string.format("%s%06d", os.date("!%m%d%H%M%S"), _M.getUsec())))
  for i = 1, in_len do
    -- printable ascii chars
    t_rand[i] = math.random(33, 126)
  end
  return string.char(table.unpack(t_rand))
end

_M.getTldsAlphaByDomain = function()
  local s_url = "http://data.iana.org/TLD/tlds-alpha-by-domain.txt"
  local b_bytecode = true
  local s_dest = "./tlds-alpha-by-domain.lua"

  local f_out = nil
  if b_bytecode then
    f_out = io.popen(string.format("luac -o %s -", s_dest), "w")
  else
    f_out = io.open(s_dest, "w")
  end
  if not(f_out) then
    io.write(string.format("ERROR opening file: %s\n", s_dest))
    return
  end
  local http = require("socket.http")
  local s_data = http.request(s_url)

  if not(s_data) then
    io.write(string.format("ERROR fetching URL: %s\n", s_url))
    return
  end
  --remove comments
  s_data = string.gsub(s_data, "#[^\r\n]*\r?[\r\n]\n?", "")
  s_data = string.lower(s_data)
  s_data = string.gsub(s_data, "([^%s]*)%s*", "%1\"]=1,[\"")
  s_data = string.gsub(s_data, ",%[\"$", "")
  s_data = string.format("return {[\"%s}", s_data)
  f_out:write(s_data)
  f_out:close()
  return s_dest
end

_M.convertUnit = function(is_convert_type, is_value, is_out_unit)
  local t_convert = {
    ["size"] = {["*"] = "KMG", K = 1024, M = 1024 * 1024, G = 1024 * 1024 * 1024},
    ["time"] = {["*"] = "mhdw", s = 1, m = 60, h = 60 * 60, d = 60 * 60 * 24, w = 60 * 60 * 24 * 7},
  }
  if not(t_convert[is_convert_type]) then
    return
  end
  is_out_unit = is_out_unit or ""
  local n_value = tonumber(is_value)
  if not(n_value) then
    n_value = tonumber((string.gsub(is_value or "", string.format("^([0-9.]+)([%s])$", t_convert[is_convert_type]["*"]), function(is_value, is_unit) return tonumber(is_value) * t_convert[is_convert_type][is_unit]; end)))
  end
  if n_value then
    if n_value == 0 then
      return "0", 0, ""
    end
    local n_out_value = n_value
    if (type(is_out_unit) == "string") and (is_out_unit == "*") then
      is_out_unit = ""
      string.gsub(t_convert[is_convert_type]["*"], "(.)", function(is_unit)
        local n_result = n_value / t_convert[is_convert_type][is_unit]
        if n_result >= 1 then
          n_out_value = n_result
          is_out_unit = is_unit
        end
      end)
    elseif t_convert[is_convert_type][is_out_unit] then
      n_out_value = n_value / t_convert[is_convert_type][is_out_unit]
    else
      is_out_unit = ""
    end
    local s_out_value = string.format("%.2f", n_out_value)
    s_out_value = string.gsub(s_out_value, "%.?0*$", "")
    return string.format("%s%s", s_out_value, is_out_unit), n_out_value, is_out_unit
  end
end

_M.connectRedis = function()
  _M.redis = _M.require("redis").connect(_M.t_config.s_redis_dsn, _M.redis_log)
  if not(_M.redis) then
    _M.redis = {closed = true}
    _M.setResult(_M.t_config.t_status_code.server_data_connection)
    return false
  elseif _M.t_config.s_redis_slave_dsn then
    _M.redis_ro = _M.require("redis").connect(_M.t_config.s_redis_slave_dsn, _M.redis_log)
    if not(_M.redis_ro) then
      _M.redis_ro = {closed = true}
      _M.setResult(_M.t_config.t_status_code.server_data_connection)
      return false
    end
  else
    _M.redis_ro = _M.redis
  end
  return true
end

_M.getRedisDefaultValue = function(is_redis_key)
  return _M.t_config.t_redis_key[is_redis_key][3]
end

_M.getRedisKey = function(is_redis_key, it_replace)
  local REDIS_SEPARATOR = "@"
  if not(_M.t_config.t_redis_key[is_redis_key]) then
    _M.log.ERROR("getRedisKey: missing key: %s", is_redis_key)
    return
  end
  local t_replace = {s_user = true, s_domain = true, s_client = true}
  local t_key = _M.t_config.t_redis_key[is_redis_key][6]
  for i = 1, #t_key do
    t_replace[i] = t_replace[t_key[i]] and (it_replace and it_replace[t_key[i]] or "") or t_key[i]
    if t_replace[i] == ""  then
      _M.log.WARN("getRedisKey: %s, missing replace: %s", is_redis_key, t_key[i])
      return
    end
  end
  local s_key = string.format("%s:%s", _M.t_config.s_redis_namespace, table.concat(t_replace, REDIS_SEPARATOR))
  _M.log.DEBUG("getRedisKey: %s = %s", is_redis_key, s_key)
  return s_key
end

_M.mgetRedis = function(it_key, it_object)
  local t_request_data = {"MGET"}
  for n_k = 1, #it_key do
    t_request_data[n_k + 1] = _M.getRedisKey(it_key[n_k], it_object)
  end
  return _M.redis_ro.q({t_request_data}, {it_key})[1]
end

_M.closeRedis = function()
  _M.redis.close()
  if _M.s_redis_slave_dsn then
    _M.redis_ro.close()
  end
end

_M.setResult = function(in_status_code, io_message)
--t_result = { status = 1 or 0, code = n, data = data and { k = v }, error = error and { message = "ERROR" } }
  local t_status_code = _M.t_config.t_status_code
  local s_log_level = "INFO"
  local s_log_error_message = (type(io_message) == "string") and io_message or ""
  local t_result = {status = in_status_code < t_status_code.error_threshold and 1 or 0}
  if  t_status_code.error_threshold <= in_status_code then
    if in_status_code < t_status_code.server then
      s_log_level = "WARN"
    else
      io_message = "Server failure."
      s_log_level = "ERROR"
    end
    if _M.redis.closed or _M.redis_ro.closed then
      in_status_code = t_status_code.server_data_connection
      s_log_error_message = string.format("data storage error... ", s_log_error_message)
    end
    t_result.code = math.floor(in_status_code)
    t_result.error = {message = io_message or ""}
    if (in_status_code == t_status_code.client_auth) or (in_status_code == t_status_code.client_auth_tmp) then
      if _M.redis_ro.q({{"EXISTS", _M.getRedisKey("config_debug_client_auth")}})[1][1] == 0 then
        t_result.code = t_status_code.unknown
        t_result.error.message = "An unknown error occurred."
        s_log_error_message = string.format("(DEBUG_CLIENT_AUTH) %s", s_log_error_message)
      end
      if (in_status_code == t_status_code.client_auth_tmp) then
        t_result.error.message = string.format("TEMP: %s", t_result.error.message)
      end
    end
  else
    t_result.data = io_message or {}
  end
  _M.log[s_log_level]("setResult: status = %d, code = %g (%s), error.message = \"%s\"", t_result.status, in_status_code, t_status_code[in_status_code], s_log_error_message)
  return t_result
end

return _M

