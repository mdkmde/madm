local _FILENAME = "madm/server.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20180925"

local _M = {}
local load, pairs, pcall, tonumber, type = load, pairs, pcall, tonumber, type
local io = {open = io.open, popen = io.popen}
local os = {date = os.date, execute = os.execute}
local string = {find = string.find, format = string.format, gsub = string.gsub, lower = string.lower, sub = string.sub}
local table = {concat = table.concat, sort = table.sort}
local tool = require("madm.tool")
_ENV = nil

_M.getVersion = function() return _FILENAME, _VERSION, _AUTHOR; end

local checkActive = function(it_object)
  local log = tool.log
  local t_request = {}
  local t_active = {
    n_client = 0,
    n_domain = -1,
    n_user = -1,
    n_status = -1,
  }
  it_object.t_active = t_active
  if it_object.s_client then
    log.DEBUG("checkActive: client - \"%s\"", it_object.s_client)
    t_request[#t_request + 1] = {"EXISTS", tool.getRedisKey("config_client_pass", it_object)}
  end
  if it_object.s_domain then
    log.DEBUG("checkActive: domain - \"%s\"", it_object.s_domain)
    t_request[#t_request + 1] = {"SISMEMBER", tool.getRedisKey("config_domain_list"), it_object.s_domain}
    t_request[#t_request + 1] = {"SISMEMBER", tool.getRedisKey("internal_config_domain_list_inactive"), it_object.s_domain}
    if it_object.s_user and (it_object.s_type == "user") then
      log.DEBUG("checkActive: user - \"%s\"", it_object.s_user)
      t_request[#t_request + 1] = {"SISMEMBER", tool.getRedisKey("domain_user_list", it_object), it_object.s_object}
      t_request[#t_request + 1] = {"SISMEMBER", tool.getRedisKey("internal_domain_user_list_inactive", it_object), it_object.s_object}
    end
  end
  if it_object.s_client and it_object.s_user then
    log.DEBUG("checkActive: user restricted clients")
    t_request[#t_request + 1] = {"EXISTS", tool.getRedisKey("user_restricted_clients", it_object)}
    t_request[#t_request + 1] = {"SISMEMBER", tool.getRedisKey("user_restricted_clients", it_object), it_object.s_client}
  end
  if 0 == #t_request then
    return t_active
  end
  local t_result = tool.redis_ro.q(t_request)
  local n_client = 0
  if it_object.s_client then
    if t_result[1][1] == 1 then
      t_active.n_client = 1
      t_active.n_status = 1
    end
    n_client = 1
  end
  if it_object.s_domain then
    if t_result[1 + n_client][1] == 1 then
      t_active.n_domain = 1
      t_active.n_status = 1
      if t_result[2 + n_client][1] == 1 then
        t_active.n_domain = 0
        t_active.n_status = 0
      end
    end
    if it_object.s_user and (it_object.s_type == "user") then
      if t_result[3 + n_client][1] == 1 then
        t_active.n_user = 1
        if t_result[4 + n_client][1] == 1 then
          t_active.n_user = 0
          t_active.n_status = 0
        end
      else
        t_active.n_status = -1
      end
    end
  end
  if it_object.s_client and it_object.s_user and (t_result[6][1] == 1) and not(t_result[7][1] == 1) then
    t_active.b_restricted_client = true
    t_active.n_status = 0
  end
end

local checkLimit = function(it_object, is_mailbox_path, ib_force_calculate_size_only)
  local log = tool.log
  local t_request_data = {"config_mailbox_limit", "config_mailbox_limit_warn", "domain_mailbox_limit", "domain_mailbox_limit_warn", "user_mailbox_limit", "user_mailbox_limit_warn", "config_mailbox_limit_subject", "config_mailbox_limit_subject_warn", "config_mailbox_limit_text", "config_mailbox_limit_sender", "user_mailbox_size", "internal_user_mailbox_limit_mail_sent", "config_mailbox_size_check_command", "config_mailbox_size_cache_interval"}
  local t_limit_data = tool.mgetRedis(t_request_data, it_object)
  local _, n_limit = tool.convertUnit("size", t_limit_data["user_mailbox_limit"] or t_limit_data["domain_mailbox_limit"] or t_limit_data["config_mailbox_limit"])
  if not(n_limit) and not(ib_force_calculate_size_only) then
    log.DEBUG("checkLimit: no limit set")
    return
  end
  log.DEBUG("checkLimit: user: \"%s\"", it_object.s_object)
  local _, n_size = tool.convertUnit("size", t_limit_data["user_mailbox_size"])
  local t_request = {}
  if not(n_size) then
    log.DEBUG("checkLimit: no limit cache - check maildirsize..")
    local f_size = io.open(string.format("%s/maildirsize", is_mailbox_path))
    if f_size then
      n_size = 0
      f_size:read("*l")
      local s_line = f_size:read("*l")
      local addSize = function(in_size) n_size = n_size + tonumber(in_size); end
      while s_line do
        string.gsub(s_line, "^([-0-9]+) .*", addSize)
        s_line = f_size:read("*l")
      end
      f_size:close()
      if n_size < 0 then
        n_size = 0
      end
      log.DEBUG("checkLimit: got maildirsize: %d", n_size)
    end
  end
  if not(n_size) then
    log.DEBUG("checkLimit: no limit cache - check now..")
    local f_p = io.popen(string.gsub(t_limit_data["config_mailbox_size_check_command"] or tool.getRedisDefaultValue("config_mailbox_size_check_command"), "%${([^}]*)}", {MAILBOX = is_mailbox_path}), "r")
    if not(f_p) then
      log.ERROR("checkLimit: cannot open check_command: \"%s\"", string.gsub(t_limit_data["config_mailbox_size_check_command"] or tool.getRedisDefaultValue("config_mailbox_size_check_command"), "%${([^}]*)}", {MAILBOX = is_mailbox_path}))
      return
    end
    n_size = tonumber(f_p:read("*a"))
    f_p:close()
    if not(n_size) then
      log.ERROR("checkLimit: cannot calculate size for: \"%s\"", is_mailbox_path)
      return
    end
    local _, n_cache_interval = tool.convertUnit("time", t_limit_data["config_mailbox_size_cache_interval"] or tool.getRedisDefaultValue("config_mailbox_size_cache_interval"))
    if n_cache_interval and (0 < n_cache_interval) then
      local s_mailbox_size_key = tool.getRedisKey("user_mailbox_size", it_object)
      t_request = {{"SET", s_mailbox_size_key, n_size}, {"EXPIRE", s_mailbox_size_key, n_cache_interval}}
      log.DEBUG("checkLimit: mailbox_size cached for %ds", n_cache_interval)
    end
    if ib_force_calculate_size_only then
      if (n_size < (n_limit or 0)) and (0 < #t_request) then
       tool.redis.q(t_request)
      end
      return {mailbox_size = n_size}
    end
  end
  local t_limit_out_data = {mailbox_size = n_size}
  local t_size_limit = {
    SIZE = tool.convertUnit("size", n_size, "*"),
    LIMIT = tool.convertUnit("size", n_limit, "*"),
  }
  t_size_limit.LIMIT_WARN = t_size_limit.LIMIT
  if n_size < n_limit then
    local _, n_limit_warn = tool.convertUnit("size", t_limit_data["user_mailbox_limit_warn"] or t_limit_data["domain_mailbox_limit_warn"] or t_limit_data["config_mailbox_limit_warn"])
    t_size_limit.LIMIT_WARN = tool.convertUnit("size", n_limit_warn or n_limit, "*")
    if n_size < (n_limit_warn or n_limit) then
      log.DEBUG("checkLimit: limit OK (%s/%s)", t_size_limit.SIZE, t_size_limit.LIMIT)
      if t_limit_data["internal_user_mailbox_limit_mail_sent"] then
        t_request[#t_request + 1] = {"DEL", tool.getRedisKey("internal_user_mailbox_limit_mail_sent", it_object)}
      end
      if 0 < #t_request then
        tool.redis.q(t_request)
      end
      return t_limit_out_data
    end
    log.DEBUG("checkLimit: limit WARN (%s/%s)", t_size_limit.SIZE, t_size_limit.LIMIT)
    t_limit_out_data.limit_subject = t_limit_data["config_mailbox_limit_subject_warn"] or tool.getRedisDefaultValue("config_mailbox_limit_subject_warn")
  else
    log.DEBUG("checkLimit: OVER limit (%s/%s)", t_size_limit.SIZE, t_size_limit.LIMIT)
    t_limit_out_data.limit_subject = t_limit_data["config_mailbox_limit_subject"] or tool.getRedisDefaultValue("config_mailbox_limit_subject")
    t_limit_out_data.limit_over_limit = 1
    t_request = {}
  end
  t_limit_out_data.limit_subject = string.gsub(t_limit_out_data.limit_subject, "%${([^}]*)}", t_size_limit)
  local s_limit_sender = t_limit_data["config_mailbox_limit_sender"] or tool.getRedisDefaultValue("config_mailbox_limit_sender")
  if not(string.find(s_limit_sender, "@")) then
    s_limit_sender = string.format("%s@%s", s_limit_sender, it_object.s_domain)
  end
  if not(t_limit_data["internal_user_mailbox_limit_mail_sent"]) or (t_limit_out_data.limit_over_limit and not(t_limit_data["internal_user_mailbox_limit_mail_sent"] == "over")) then
    local s_sender, n_match = string.gsub(it_object.s_option or "", "^limit_mail(<[^ >]*>)$", "%1")
    if t_limit_data["internal_user_mailbox_limit_mail_sent"] and not(s_sender == "") and string.find(t_limit_data["internal_user_mailbox_limit_mail_sent"], s_sender) then
      t_request = {{"SET", tool.getRedisKey("internal_user_mailbox_limit_mail_sent", it_object), "over"}}
      log.DEBUG("checkLimit: OVER limit - mailbox_size cache deleted")
    elseif n_match == 1 then
      t_limit_out_data.limit_sender = s_limit_sender
      t_limit_out_data.limit_text = string.gsub(t_limit_data["config_mailbox_limit_text"] or "", "%${([^}]*)}", t_size_limit)
      if t_limit_out_data.limit_over_limit then
        t_request = {{"SET", tool.getRedisKey("internal_user_mailbox_limit_mail_sent", it_object), string.format("%s %s ", t_limit_data["internal_user_mailbox_limit_mail_sent"] or "", s_sender)}}
      else
        t_request[#t_request + 1] = {"SET", tool.getRedisKey("internal_user_mailbox_limit_mail_sent", it_object), "warn"}
      end
      log.INFO("checkLimit: \"%s\" hit the mark (%s/%s%s) - return limit mail data", it_object.s_object, t_size_limit.SIZE, t_size_limit.LIMIT, t_limit_out_data.limit_over_limit and "" or " - WARN")
    else
      t_limit_out_data.limit_mail = 1
    end
    t_limit_out_data.limit_over_limit = nil
  end
  if 0 < #t_request then
    tool.redis.q(t_request)
  end
  return t_limit_out_data
end

local getMailbox = function(it_object, ib_force_calculate_size_only, ib_force_raw_path_only)
  local mkdir_recurse = function(is_dir) end
  mkdir_recurse = function(is_dir)
    if is_dir == "" then
      return
    elseif (tool.fileExists(is_dir) == tool.TYPE_DIR) then
      return true
    else
      mkdir_recurse(string.gsub(is_dir, "/[^/]*$", ""))
    end
    return tool.mkdir(is_dir)
  end
  local log = tool.log
  -- inactive
  if not(it_object.t_active.n_status) then
    checkActive(it_object)
  end
  if not(it_object.t_active.n_status == 1) and not(ib_force_raw_path_only) then
    return
  end
  local t_request_data = {"user_mailbox_path", "domain_mailbox_path", "config_mailbox_path", "user_mailbox", "user_mailbox_type", "domain_mailbox_type", "config_mailbox_type"}
  local t_mailbox_data = tool.mgetRedis(t_request_data, it_object)
  if not(t_mailbox_data["user_mailbox"]) then
    return
  end
  local s_mailbox_type = t_mailbox_data["user_mailbox_type"] or t_mailbox_data["domain_mailbox_type"] or t_mailbox_data["config_mailbox_type"] or tool.t_config.t_mailbox_type[1]
  log.DEBUG("getMailbox: type for \"%s\" is: \"%s\"", it_object.s_object, s_mailbox_type)
  local s_mailbox_path = it_object.s_user
  local b_relative_path = true
  if t_mailbox_data["user_mailbox_path"] then
    s_mailbox_path = t_mailbox_data["user_mailbox_path"]
    if string.find(t_mailbox_data["user_mailbox_path"], "^|?/") then
      b_relative_path = false
    end
  end
  if t_mailbox_data["domain_mailbox_path"] then
    s_mailbox_path = string.format("%s/%s", t_mailbox_data["domain_mailbox_path"], s_mailbox_path)
    if string.find(t_mailbox_data["domain_mailbox_path"], "^/") then
      b_relative_path = false
    end
  elseif b_relative_path then
    s_mailbox_path = string.format("%s/%s", it_object.s_domain, s_mailbox_path)
  end
  if t_mailbox_data["config_mailbox_path"] and b_relative_path then
    s_mailbox_path = string.format("%s/%s", t_mailbox_data["config_mailbox_path"], s_mailbox_path)
  end
  -- insert first slash, remove multi slash and last slash
  s_mailbox_path = string.gsub(string.gsub(string.format("/%s", s_mailbox_path), "///*", "/"), "/$", "")
  log.DEBUG("getMailbox: path for \"%s\" is: \"%s\"", it_object.s_object, s_mailbox_path)
  if ib_force_raw_path_only then
    return nil, nil, s_mailbox_path
  end
  if s_mailbox_type == "mbox" then
    if not(io.open(s_mailbox_path, "a")) then
      if not(mkdir_recurse(string.gsub(s_mailbox_path, "/[^/]*$", ""))) or not(io.open(s_mailbox_path, "a")) then
        return nil, nil, s_mailbox_path
      end
    end
  elseif s_mailbox_type == "maildir" then
    if not(tool.fileExists(s_mailbox_path) == tool.TYPE_DIR) then
      for _, s_dir in pairs({"new", "cur", "tmp"}) do
        if not(mkdir_recurse(string.format("%s/%s", s_mailbox_path, s_dir))) then
          return nil, nil, s_mailbox_path
        end
      end
    end
  else
    log.ERROR("getMailbox: unknown mailbox_type: \"%s\"",s_mailbox_type)
  end
  local t_limit_data
  if not(string.find(s_mailbox_path, "^|")) then
    t_limit_data = checkLimit(it_object, s_mailbox_path, ib_force_calculate_size_only)
  end
  return s_mailbox_path, t_limit_data
end

local getObject = function(it_request)
  local log = tool.log
  local r = tool.redis
  local r_ro = tool.redis_ro
  local t_redis_request = {}
  local t_result = {}
  for n_k = 1, #it_request.t_data.t_attributes do
    log.DEBUG("getObject: got attribute: \"%s\" (%s)", it_request.t_data.t_redis_key[n_k], it_request.t_data.t_type[n_k])
    if it_request.t_data.t_type[n_k] == "table" then
      t_redis_request[n_k] = {"SMEMBERS", tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object)}
    else
      if it_request.t_data.t_attributes[n_k] == "active" then
        t_redis_request[n_k] = {"SISMEMBER", tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object), it_request.t_object.s_object}
      elseif string.find(it_request.t_data.t_redis_key[n_k], "_list") then
        if string.find(it_request.t_data.t_attributes[n_k], "_list_active$") then
          t_redis_request[n_k] = {"SDIFF", tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object), tool.getRedisKey(string.format("internal_%s_inactive", it_request.t_data.t_redis_key[n_k]), it_request.t_object)}
        elseif string.find(it_request.t_data.t_attributes[n_k], "_list_inactive$") then
          t_redis_request[n_k] = {"SMEMBERS", tool.getRedisKey(string.format("internal_%s_inactive", it_request.t_data.t_redis_key[n_k]), it_request.t_object)}
        else
          t_redis_request[n_k] = {"SMEMBERS", tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object)}
        end
        it_request.t_data.t_type[n_k] = "table"
      elseif it_request.t_data.t_attributes[n_k] == "autore_active" then
        t_redis_request[n_k] = {"MGET", tool.getRedisKey("user_autore_start", it_request.t_object), tool.getRedisKey("user_autore_end", it_request.t_object)}
        it_request.t_data.t_type[n_k] = "table"
      else
        t_redis_request[n_k] = {"GET", tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object)}
      end
    end
  end
  t_result = r_ro.q(t_redis_request)
  for n_k = 1, #t_result do
    if it_request.t_data.t_type[n_k] == "table" then
      t_result[it_request.t_data.t_attributes[n_k]] = t_result[n_k] or tool.getRedisDefaultValue(it_request.t_data.t_redis_key[n_k])
      table.sort(t_result[it_request.t_data.t_attributes[n_k]], function(a, b) if not(a) or not(b) then return true; else return a < b; end; end)
    elseif it_request.t_data.t_type[n_k] == "string" then
      t_result[it_request.t_data.t_attributes[n_k]] = t_result[n_k][1] or tool.getRedisDefaultValue(it_request.t_data.t_redis_key[n_k])
    elseif it_request.t_data.t_type[n_k] == "number" then
      t_result[it_request.t_data.t_attributes[n_k]] = tonumber(t_result[n_k][1]) or tool.getRedisDefaultValue(it_request.t_data.t_redis_key[n_k])
    end
    t_result[n_k] = nil
  end
  if t_result["active"] then
    t_result["active"] = it_request.t_object.s_client and it_request.t_object.t_active.n_client or it_request.t_object.t_active.n_status
  end
  if t_result["user"] then
    t_result["user"] = it_request.t_object.s_object or ""
  end
  if t_result["domain"] then
    t_result["domain"] = it_request.t_object.s_domain or ""
  end
  if t_result["autore_active"] then
    local n_cur_date = tonumber(os.date("!%Y%m%d"))
    if (t_result["autore_active"][1] or t_result["autore_active"][2]) and ((tonumber(t_result["autore_active"][1]) or 0) <= n_cur_date) and (n_cur_date <= (tonumber(t_result["autore_active"][2]) or 100000000)) then
      if t_result["autore_active"][1] and t_result["autore_active"][2] then
        r.q({{"DEL", tool.getRedisKey("user_autore_start", it_request.t_object)}})
      end
      t_result["autore_active"] = 1
      if it_request.t_data.s_option then
        local t_mail_from = tool.checkUserDomain(it_request.t_data.s_option)
        if t_mail_from.s_object then
          t_result["autore_active"] = (r.q({{"SISMEMBER", tool.getRedisKey("internal_user_autore_sent_to", it_request.t_object), t_mail_from.s_object}, {"SADD", tool.getRedisKey("internal_user_autore_sent_to", it_request.t_object), t_mail_from.s_object}})[1][1] == 0) and 1 or nil
        end
      end
    else
      if not(t_result["autore_active"][1]) and t_result["autore_active"][2] then
        r.q({{"DEL", tool.getRedisKey("user_autore_end", it_request.t_object), tool.getRedisKey("internal_user_autore_sent_to", it_request.t_object)}})
      elseif (tonumber(t_result["autore_active"][2]) or 1) < (tonumber(t_result["autore_active"][1]) or 0) then
        r.q({{"DEL", tool.getRedisKey("user_autore_start", it_request.t_object), tool.getRedisKey("user_autore_end", it_request.t_object), tool.getRedisKey("internal_user_autore_sent_to", it_request.t_object)}})
      end
      t_result["autore_active"] = nil
    end
  end
  if t_result["autore_reply_as"] and (t_result["autore_reply_as"] == "") then
    t_result["autore_reply_as"] = it_request.t_object.s_object
  end
  if t_result["client_list"] then
    for n_k = 1, #t_result["client_list"] do
      t_result["client_list"][n_k] = string.format("%s@", t_result["client_list"][n_k])
    end
  end
  if t_result["restricted_clients"] then
    for n_k = 1, #t_result["restricted_clients"] do
      t_result["restricted_clients"][n_k] = string.format("%s@", t_result["restricted_clients"][n_k])
    end
  end
  if t_result["pass"] then
    if (it_request.t_object.s_client and it_result.t_object.t_active.n_client or it_request.t_object.t_active.n_status) == 1 then
      t_result["pass"] = tool.crypt(t_result["pass"], tool.t_config.DATA_MASTER_PASSWORD, tool.DECRYPT, "x")
    else
      t_result["pass_raw"] = tool.crypt(t_result["pass"], tool.t_config.DATA_MASTER_PASSWORD, tool.DECRYPT, "x")
      t_result["pass"] = nil
    end
    log.DEBUG("getObject: READ_PASS: \"%s\" (%spass)", it_request.t_object.s_object, it_request.t_object.s_client and "client " or "")
  end
  if t_result["mailbox_data"] then
    local t_limit_data
    t_result["mailbox_data"], t_limit_data, t_result["mailbox_data_path"] = getMailbox(it_request.t_object)
    for s_k, s_v in pairs(t_limit_data or {}) do
      t_result[s_k] = s_v
    end
    local t_mailbox_data = tool.mgetRedis({"config_mailbox_type", "domain_mailbox_type", "user_mailbox_type"}, it_request.t_object)
    t_result["mailbox_data_type"] = t_mailbox_data["user_mailbox_type"] or t_mailbox_data["domain_mailbox_type"] or t_mailbox_data["config_mailbox_type"] or tool.t_config.t_mailbox_type[1]
    if it_request.t_data.s_option and (it_request.t_data.s_option == "update_last_access") then
      r.q({{"SET", tool.getRedisKey("user_last_access", it_request.t_object), os.date("!%Y-%m-%dT%H:%MZ")}})
      log.INFO("ACCESS mailbox: \"%s\"", it_request.t_object.s_object)
    end
  end
  local t_limit_check_result_data = {
    mailbox_limit = {"config_mailbox_limit", "domain_mailbox_limit", "user_mailbox_limit", "mailbox_limit_inherited", "mailbox_limit_raw"},
    mailbox_limit_max = {"config_mailbox_limit_max", "domain_mailbox_limit_max", nil, "mailbox_limit_max_inherited", "mailbox_limit_max_raw"},
    mailbox_limit_warn = {"config_mailbox_limit_warn", "domain_mailbox_limit_warn", "user_mailbox_limit_warn", "mailbox_limit_warn_inherited", "mailbox_limit_warn_raw"},
  }
  for s_limit_name, t_limit_data in pairs(t_limit_check_result_data) do
    if (type(t_result[s_limit_name]) == "boolean") or (t_result["mailbox_data"]) then
      local t_mailbox_limit, _ = tool.mgetRedis({t_limit_data[1], t_limit_data[2], (it_request.t_object.s_type == "user") and t_limit_data[3] or nil}, it_request.t_object)
      t_result[s_limit_name] = t_mailbox_limit[t_limit_data[3]] or t_mailbox_limit[t_limit_data[2]] or t_mailbox_limit[t_limit_data[1]] or nil
      if t_result[s_limit_name] then
        if not(t_mailbox_limit[t_limit_data[3]]) then
          t_result[t_limit_data[4]] = 1
        end
        if t_result["mailbox_data"] then
          t_result[t_limit_data[5]] = t_result[s_limit_name]
        end
      end
    end
    if t_result[s_limit_name] then
      t_result[string.format("%s_raw", s_limit_name)] = t_result[s_limit_name]
      t_result[s_limit_name] = tool.convertUnit("size", t_result[s_limit_name], "*")
    end
  end
  if t_result["mailbox_size"] then
    if type(t_result["mailbox_size"]) == "boolean" then
      local _, t_limit_data = getMailbox(it_request.t_object, true)
      t_result["mailbox_size"] = t_limit_data and t_limit_data.mailbox_size
    end
    if t_result["mailbox_size"] then
      t_result["mailbox_size_raw"] = t_result["mailbox_size"]
      t_result["mailbox_size"] = tool.convertUnit("size", t_result["mailbox_size"], "*")
    end
  end
  if t_result["recipient_data"] then
    log.DEBUG("getObject: request recipient data")
    local appendAlias = function(it_alias, is_address)
      for _, s_v in pairs(it_alias) do
        if is_address == s_v then return; end
      end
      it_alias[#it_alias + 1] = is_address
    end
    local t_alias_request = {it_request.t_object.s_object}
    local t_alias_result = {}
    local t_mailbox_path_raw = {}
    local n_alias = 1
    local b_recipient_has_mailbox = false
    while t_alias_request[n_alias] do
      local t_alias = tool.checkUserDomain(t_alias_request[n_alias])
      log.DEBUG("getObject: check alias \"%s\"", t_alias.s_object)
      local s_domain_alias = r_ro.q({{"GET", tool.getRedisKey("domain_alias", t_alias)}})[1][1]
      if s_domain_alias then
        t_alias.s_domain = s_domain_alias
        t_alias.s_object = string.format("%s@%s", t_alias.s_user, t_alias.s_domain)
        log.DEBUG("getObject: domain alias: \"%s\"", t_alias.s_object)
      end
      checkActive(t_alias)
      local t_alias_data = tool.mgetRedis({"user_mailbox", "domain_catchall"}, t_alias)
      if not(t_alias.t_active.n_domain == 1) and (1 < n_alias) then
        log.DEBUG("getObject: external alias: \"%s\"", t_alias.s_object)
        appendAlias(t_alias_result, t_alias.s_object)
      elseif t_alias.t_active.n_domain == 1 then
        if t_alias.t_active.n_user == 1 then
          if t_alias_data["user_mailbox"] then
            if n_alias == 1 then
              b_recipient_has_mailbox = true
            end
            t_alias.s_option = it_request.t_data.s_option
            local s_mailbox_path, t_limit_data, s_mailbox_path_raw = getMailbox(t_alias)
            if s_mailbox_path and (not(t_limit_data) or not(t_limit_data.limit_over_limit)) then
              log.DEBUG("getObject: user mailbox: \"%s\"", s_mailbox_path)
              appendAlias(t_alias_result, s_mailbox_path)
            elseif s_mailbox_path_raw then
              appendAlias(t_mailbox_path_raw, s_mailbox_path_raw)
            end
            if t_limit_data then
              log.DEBUG("getObject: user mailbox limit: \"%s\"", t_limit_data.subject)
              if n_alias == 1 then
                for s_k, s_v in pairs(t_limit_data) do
                  t_result[s_k] = s_v
                end
              end
            end
          end
          local t_alias_list = r_ro.q({{"SMEMBERS", tool.getRedisKey("user_aliases", t_alias)}})[1]
          log.DEBUG("getObject: user aliases: \"%d\"", #t_alias_list)
          for _, s_v in pairs(t_alias_list) do
            appendAlias(t_alias_request, s_v)
          end
        elseif not(t_alias.t_active.n_user == 1) and t_alias_data["domain_catchall"] then
          log.DEBUG("getObject: domain catchall: \"%s\"", t_alias_data["domain_catchall"])
          appendAlias(t_alias_request, t_alias_data["domain_catchall"])
        end
      end
      n_alias = n_alias + 1
    end
    t_result["recipient_data"] = t_alias_result
    if (0 < #t_result["recipient_data"]) and not(b_recipient_has_mailbox) and (it_request.t_data.s_option == "update_last_access") then
      r.q({{"SET", tool.getRedisKey("user_last_access", it_request.t_object), os.date("!%Y-%m-%dT%H:%MZ")}})
    end
    if 0 < #t_mailbox_path_raw then
      t_result["recipient_data_mailbox_path_raw"] = t_mailbox_path_raw
    end
  end
  return tool.setResult(tool.t_config.t_status_code.ok, t_result)
end
tool.t_config.t_action.get = getObject

local setObject = function(it_request)
  local log = tool.log
  local t_reverse = {
    user_aliases = "user_internal_alias_from_list",
    user_internal_alias_from_list = "user_aliases",
    domain_admin_users = "user_admin_domains",
    user_admin_domains = "domain_admin_users",
  }
  local t_redis_request = {}
  local s_redis_cmd
  local t_limit = {}
  for n_k = 1, #it_request.t_data.t_attributes do
    log.DEBUG("setObject: got attribute: \"%s\" (%s)", it_request.t_data.t_attributes[n_k], it_request.t_data.t_type[n_k])
    if it_request.t_data.t_type[n_k] == "table" then
      s_redis_cmd = "SADD"
      for _, t_v in pairs(it_request.t_data.t_values[n_k]) do
        if type(t_v) == "table" then
          log.DEBUG("setObject: %s \"%s\" \"%s\"", s_redis_cmd, it_request.t_data.t_redis_key[n_k], t_v.s_object)
          t_redis_request[#t_redis_request + 1] = {s_redis_cmd, tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object), t_v.s_object}
          local s_flag, n_match = string.gsub(it_request.t_data.t_redis_key[n_k], "^config_admin_flag_(.+)_users$", "%1")
          if t_reverse[it_request.t_data.t_redis_key[n_k]] then
            t_redis_request[#t_redis_request + 1] = {s_redis_cmd, tool.getRedisKey(t_reverse[it_request.t_data.t_redis_key[n_k]], t_v), it_request.t_object.s_object}
          elseif it_request.t_data.t_redis_key[n_k] == "user_admin_flags" then
            t_redis_request[#t_redis_request + 1] = {s_redis_cmd, tool.getRedisKey(string.format("config_admin_flag_%s_users", t_v.s_object), it_request.t_object), it_request.t_object.s_object}
          elseif (n_match == 1) and tool.t_config.t_admin_flag[s_flag] then
            t_redis_request[#t_redis_request + 1] = {s_redis_cmd, tool.getRedisKey("user_admin_flags", t_v), tool.t_config.t_admin_flag[s_flag]}
          end
        else
          s_redis_cmd = (t_v == "-") and "SREM" or "SADD"
        end
      end
    else
      local s_value = it_request.t_data.t_values[n_k]
      s_redis_cmd = "SET"
      if it_request.t_data.t_attributes[n_k] == "active" then
        -- domain|user _active
        if s_value == "0" then
          s_redis_cmd = "SADD"
        else
          s_redis_cmd = "SREM"
        end
        s_value = it_request.t_object.s_object
        it_request.t_data.t_redis_key[n_k] = string.format("internal_%s_inactive", it_request.t_data.t_redis_key[n_k])
      elseif it_request.t_data.t_redis_key[n_k] == "user_pass" then
        s_value = tool.crypt(s_value, tool.t_config.DATA_MASTER_PASSWORD, tool.ENCRYPT, "x")
      elseif (it_request.t_data.t_redis_key[n_k] == "config_client_ip") and not(string.find(s_value, "^[0-9]+%.[0-9]+%.[0-9]+%.[0-9]+$")) then
          s_redis_cmd = "DEL"
          s_value = nil
      elseif it_request.t_data.t_redis_key[n_k] == "config_client_pass" then
        if s_value == "" then
          t_redis_request[#t_redis_request + 1] = {"SREM", tool.getRedisKey("config_client_list", it_request.t_object), it_request.t_object.s_object}
          s_redis_cmd = "DEL"
          s_value = tool.getRedisKey("config_client_ip", it_request.t_object)
        else
          t_redis_request[#t_redis_request + 1] = {"SADD", tool.getRedisKey("config_client_list", it_request.t_object), it_request.t_object.s_object}
          s_value = tool.crypt(s_value, tool.t_config.DATA_MASTER_PASSWORD, tool.ENCRYPT, "x")
        end
      elseif tool.t_config.t_redis_key[it_request.t_data.t_redis_key[n_k]][4] then
        if s_value == tool.t_config.t_redis_key[it_request.t_data.t_redis_key[n_k]][4] then
          s_redis_cmd = "DEL"
          if it_request.t_data.t_redis_key[n_k] == "user_autore_start" then
            s_value = tool.getRedisKey("internal_user_autore_sent_to", it_request.t_object)
          else
            s_value = nil
          end
        elseif tool.t_config.t_redis_key[it_request.t_data.t_redis_key[n_k]][5] then
          s_value = "1"
        elseif string.find(it_request.t_data.t_redis_key[n_k], "limit$") then
          local _, n_max_limit
          if not(it_request.t_sender.s_has_permission == tool.t_config.t_admin_flag.config) then
            local t_request_data = {"config_mailbox_limit_max", not(it_request.t_sender.s_has_permission == tool.t_config.t_admin_flag.global_domain) and "domain_mailbox_limit_max" or nil}
            local t_limit_data = tool.mgetRedis(t_request_data, it_request.t_object)
            _, n_max_limit = tool.convertUnit("size", t_limit_data["domain_mailbox_limit_max"] or t_limit_data["config_mailbox_limit_max"])
          end
          if not(n_max_limit) or (it_request.t_data.t_values[n_k] < n_max_limit) then
            n_max_limit = it_request.t_data.t_values[n_k]
          else
            log.NOTICE("setObject: \"%d\" set to max limit: \"%d\" for \"%s\"", it_request.t_data.t_values[n_k], n_max_limit, it_request.t_data.t_attributes[n_k])
          end
          s_value = tool.convertUnit("size", n_max_limit)
          if t_limit.warn then
            if (t_limit.warn[2] < n_max_limit) then
              t_redis_request[t_limit.warn[1]][3] = tool.convertUnit("size", t_limit.warn[2])
            else
              t_redis_request[t_limit.warn[1]][3] = tool.convertUnit("size", n_max_limit)
              log.NOTICE("setObject: \"%d\" set to max limit: \"%d\" for \"%s\"", t_limit.warn[2], n_max_limit, t_limit.warn[3])
            end
          else
            t_limit.limit = n_max_limit
          end
          if string.find(it_request.t_data.t_redis_key[n_k], "^user") then
            t_redis_request[#t_redis_request + 1] = {"DEL", tool.getRedisKey("internal_user_mailbox_limit_mail_sent", it_request.t_object)}
          end
        elseif string.find(it_request.t_data.t_redis_key[n_k], "limit_max$") and it_request.t_sender.s_has_permission == tool.t_config.t_admin_flag.global_domain then
          local _, n_max_limit = tool.convertUnit("size", tool.redis_ro.q({{"GET", tool.getRedisKey("config_mailbox_limit_max", it_request.t_sender)}})[1][1])
          if (it_request.t_data.t_values[n_k] < n_max_limit) then
            s_value = tool.convertUnit("size", it_request.t_data.t_values[n_k])
          else
            s_value = tool.convertUnit("size", n_max_limit)
            log.NOTICE("setObject: \"%d\" set to max limit: \"%d\" for \"%s\"", it_request.t_data.t_values[n_k], n_max_limit, it_request.t_data.t_attributes[n_k])
          end
        elseif string.find(it_request.t_data.t_redis_key[n_k], "limit_warn$") then
          local _, n_max_limit = nil, t_limit.limit
          if not(n_max_limit) then
            local t_request_data = {"config_mailbox_limit", not(it_request.t_object.s_type == "config") and "domain_mailbox_limit" or nil, it_request.t_object.s_type == "user" and "user_mailbox_limit" or nil}
            local t_limit_data = tool.mgetRedis(t_request_data, it_request.t_object)
            _, n_max_limit = tool.convertUnit("size", t_limit_data["user_mailbox_limit"] or t_limit_data["domain_mailbox_limit"] or t_limit_data["config_mailbox_limit"])
            if it_request.t_object.s_type == "user" then
              t_redis_request[#t_redis_request + 1] = {"DEL", tool.getRedisKey("internal_user_mailbox_limit_mail_sent", it_request.t_object)}
            end
            t_limit.warn = {#t_redis_request + 1, it_request.t_data.t_values[n_k], it_request.t_data.t_attributes[n_k]}
          end
          if not(n_max_limit) or (it_request.t_data.t_values[n_k] < n_max_limit) then
            s_value = tool.convertUnit("size", it_request.t_data.t_values[n_k])
          else
            s_value = tool.convertUnit("size", n_max_limit)
            log.NOTICE("setObject: \"%d\" set to max limit: \"%d\" for \"%s\"", it_request.t_data.t_values[n_k], n_max_limit, it_request.t_data.t_attributes[n_k])
          end
        end
      end
      log.DEBUG("setObject: %s \"%s\" \"%s\"", s_redis_cmd, it_request.t_data.t_attributes[n_k], s_value or "nil")
      t_redis_request[#t_redis_request + 1] = {s_redis_cmd, tool.getRedisKey(it_request.t_data.t_redis_key[n_k], it_request.t_object), s_value}
    end
  end
  tool.redis.q(t_redis_request)
  if tool.t_config.redis_save then
    local b_status, s_err = pcall(tool.redis.q, tool.redis, {{tool.t_config.redis_save}})
    if not(b_status) then
      return tool.setResult(tool.t_config.t_status_code.server_data_save, string.format("OK, data set. Data storage \"%s\" failed: \"%s\"", tool.t_config.redis_save, s_err))
    end
  end
  return tool.setResult(tool.t_config.t_status_code.ok)
end
tool.t_config.t_action.set = setObject

local newObject = function(it_request)
  local s_object_list = "config_domain_list"
  if it_request.t_object.s_type == "user" then
    s_object_list = "domain_user_list"
    if it_request.t_object.t_active.n_domain == -1 then
      return tool.setResult(tool.t_config.t_status_code.client, string.format("newObject: parent domain \"%s\" does not exist", it_request.t_object.s_domain))
    end
    tool.log.DEBUG("newObject: parent domain does exist")
  end
  if it_request.t_object.t_active.n_status == 1 then
    return tool.setResult(tool.t_config.t_status_code.client, string.format("%s exists", it_request.t_object.s_type))
  end
  tool.log.DEBUG("newObject: %s \"%s\" does not exist .. create new", it_request.t_object.s_type, it_request.t_object.s_object)
  tool.redis.q({{"SADD", tool.getRedisKey(s_object_list, it_request.t_object), it_request.t_object.s_object}, {"SET", tool.getRedisKey(string.format("%s_created", it_request.t_object.s_type), it_request.t_object), os.date("!%Y-%m-%dT%H:%MZ")}})
  return setObject(it_request)
end
tool.t_config.t_action.new = newObject

local delObject = function(it_request)
  local r_ro = tool.redis_ro
  local log = tool.log
  local s_object_list = "domain_user_list"
  if it_request.t_object.s_type == "domain" then
    if it_request.t_object.s_object == "localhost" then
      return tool.setResult(tool.t_config.t_status_code.client, string.format("cannot remove domain: \"%s\"", it_request.t_object.s_object))
    end
    s_object_list = "config_domain_list"
    if r_ro.q({{"EXISTS", tool.getRedisKey("domain_user_list", it_request.t_object)}})[1][1] == 1 then
      return tool.setResult(tool.t_config.t_status_code.client, "users exists for domain")
    end
    tool.log.DEBUG("delObject: no user exists for domain")
  else
    local t_result = r_ro.q({{"SISMEMBER", tool.getRedisKey("user_admin_flags", it_request.t_object), tool.t_config.t_admin_flag.config}, {"GET", tool.getRedisKey("config_mailbox_delete_command")}, {"GET", tool.getRedisKey("config_mailbox_delete_path_min_length")}, {"GET", tool.getRedisKey("user_mailbox", it_request.t_object)}})
    if t_result[1][1] == 1 then
      return tool.setResult(tool.t_config.t_status_code.client, "user has config flag")
    end
    if t_result[4][1] then
      local _, _, s_mailbox_path = getMailbox(it_request.t_object, nil, true)
      if s_mailbox_path then
        local s_mailbox_delete_command, n_ok = string.gsub(t_result[2][1] or tool.getRedisDefaultValue("config_mailbox_delete_command"), "%${MAILBOX}", s_mailbox_path)
        if #s_mailbox_path < (tonumber(t_result[3][1]) or tool.getRedisDefaultValue("config_mailbox_delete_path_min_length")) then
          log.WARN("delObject: mailbox delete denied, path to short: \"%s\" < %d characters", s_mailbox_path, (tonumber(t_result[3][1]) or tool.getRedisDefaultValue("config_mailbox_delete_path_min_length")))
        elseif n_ok == 1 then
          if os.execute(s_mailbox_delete_command) then
            log.INFO("delObject: mailbox deleted for: \"%s\" (%s)", it_request.t_object.s_object, s_mailbox_path)
          else
            log.WARN("delObject: mailbox delete failed for: \"%s\" (%s)", it_request.t_object.s_object, s_mailbox_path)
          end
        else
          log.WARN("delObject: mailbox delete failed, invalid command: \"%s\"", s_mailbox_delete_command)
        end
      else
        log.WARN("delObject: cannot read/delete mailbox for: \"%s\"", it_request.t_object.s_object)
      end
    else
      log.DEBUG("delObject: no mailbox for: \"%s\"", it_request.t_object.s_object)
    end
  end
  local t_data = {
    t_redis_key = {},
    t_type = {},
    t_attributes = {},
    t_values = {},
  }
  it_request.t_data = t_data
  local t_del_all = {"DEL"}
  for s_k, t_v in pairs(tool.t_config.t_redis_key) do
    if string.find(string.gsub(s_k, "^internal_", ""), string.format("^%s_", it_request.t_object.s_type)) then
      if t_v[2] == "table" then
        local t_result = r_ro.q({{"SMEMBERS", tool.getRedisKey(s_k, it_request.t_object)}})[1]
        if 0 < #t_result then
          t_data.t_attributes[#t_data.t_attributes + 1] = string.format("DEL_%s", s_k)
          t_data.t_redis_key[#t_data.t_attributes] = s_k
          t_data.t_values[#t_data.t_attributes] = {"-"}
          for n_k = 1, #t_result do
            t_data.t_values[#t_data.t_attributes][n_k + 1] = tool.checkUserDomain(t_result[n_k])
            -- for admin flags / s_object will be empty
            t_data.t_values[#t_data.t_attributes][n_k + 1].s_object = t_result[n_k]
          end
          t_data.t_type[#t_data.t_attributes] = "table"
        end
      else
        t_del_all[#t_del_all + 1] = tool.getRedisKey(s_k, it_request.t_object)
      end
    end
  end
  tool.redis.q({t_del_all, {"SREM", tool.getRedisKey(s_object_list, it_request.t_object), it_request.t_object.s_object}, {"SREM", tool.getRedisKey(string.format("internal_%s_inactive", s_object_list), it_request.t_object), it_request.t_object.s_object}})
  log.DELETED_OBJECT("delObject: deleted: \"%s\" (by \"%s\")", it_request.t_object.s_object, it_request.t_sender.s_object)
  if 0 < #t_data.t_attributes then
    return setObject(it_request)
  end
  return tool.setResult(tool.t_config.t_status_code.ok)
end
tool.t_config.t_action.del = delObject

local movUser = function(it_request)
  local r_ro = tool.redis_ro
  local log = tool.log
  local t_destination = it_request.t_data.t_destination
  if t_destination.t_active.n_status == 1 then
    return tool.setResult(tool.t_config.t_status_code.client, string.format("destination user \"%s\" already exists", t_destination.s_object))
  end
  tool.log.DEBUG("movUser: User \"%s\" does not exist .. move from \"%s\"", t_destination.s_object, it_request.t_object.s_object)
  local _, _, s_mailbox_path_src = getMailbox(it_request.t_object, nil, true)
  local t_pre_request = {{"GET", tool.getRedisKey("config_mailbox_move_command")}, {"GET", tool.getRedisKey("user_mailbox", it_request.t_object)}, {"SISMEMBER", tool.getRedisKey("internal_domain_user_list_inactive", it_request.t_object), it_request.t_object.s_object}}
  local t_data_src = {
    t_redis_key = {},
    t_type = {},
    t_attributes = {},
    t_values = {},
  }
  local t_data_dst = {
    t_redis_key = {},
    t_type = {},
    t_attributes = {},
    t_values = {},
  }
  for s_k, t_v in pairs(tool.t_config.t_redis_key) do
    if string.find(string.gsub(s_k, "^internal_", ""), "^user_") then
      if t_v[2] == "table" then
        local t_result = r_ro.q({{"SMEMBERS", tool.getRedisKey(s_k, it_request.t_object)}})[1]
        if 0 < #t_result then
          t_data_src.t_attributes[#t_data_src.t_attributes + 1] = string.format("MOV_SRC_%s", s_k)
          t_data_dst.t_attributes[#t_data_dst.t_attributes + 1] = string.format("MOV_DST_%s", s_k)
          t_data_src.t_redis_key[#t_data_src.t_attributes] = s_k
          t_data_dst.t_redis_key[#t_data_dst.t_attributes] = s_k
          t_data_src.t_values[#t_data_src.t_attributes] = {"-"}
          t_data_dst.t_values[#t_data_dst.t_attributes] = {"+"}
          for n_k = 1, #t_result do
            t_data_src.t_values[#t_data_src.t_attributes][n_k + 1] = tool.checkUserDomain(t_result[n_k])
            t_data_dst.t_values[#t_data_dst.t_attributes][n_k + 1] = tool.checkUserDomain(t_result[n_k])
            -- for admin flags / s_object will be empty
            t_data_src.t_values[#t_data_src.t_attributes][n_k + 1].s_object = t_result[n_k]
            t_data_dst.t_values[#t_data_dst.t_attributes][n_k + 1].s_object = t_result[n_k]
          end
          t_data_src.t_type[#t_data_src.t_attributes] = "table"
          t_data_dst.t_type[#t_data_dst.t_attributes] = "table"
        end
      else
        t_pre_request[#t_pre_request + 1] = {"EXISTS", tool.getRedisKey(s_k, it_request.t_object), tool.getRedisKey(s_k, t_destination)}
      end
    end
  end
  local t_result = r_ro.q(t_pre_request)
  local s_mailbox_move_command = t_result[1][1]
  local b_mailbox = t_result[2][1]
  local b_user_inactive = (t_result[3][1] == 1)
  local t_request = {}
  for n_k = 4, #t_result do
    if t_result[n_k][1] == 1 then
      t_request[#t_request + 1] = t_pre_request[n_k]
      t_request[#t_request][1] = "RENAME"
    end
  end
  t_request[#t_request + 1] = {"SREM", tool.getRedisKey("domain_user_list", it_request.t_object), it_request.t_object.s_object}
  t_request[#t_request + 1] = {"SADD", tool.getRedisKey("domain_user_list", t_destination), t_destination.s_object}
  if b_user_inactive then
    t_request[#t_request + 1] = {"SREM", tool.getRedisKey("internal_domain_user_list_inactive", it_request.t_object), it_request.t_object.s_object}
    t_request[#t_request + 1] = {"SADD", tool.getRedisKey("internal_domain_user_list_inactive", t_destination), t_destination.s_object}
  end
  tool.redis.q(t_request)
  if 0 < #t_data_src.t_attributes then
    it_request.t_data = t_data_src
    setObject(it_request)
    local t_object = it_request.t_object
    it_request.t_object = t_destination
    it_request.t_data = t_data_dst
    setObject(it_request)
    it_request.t_object = t_object
  end
  if t_destination.b_set_alias then
    checkActive(t_destination)
    local t_data = {
      t_redis_key = {"user_aliases"},
      t_attributes = {"aliases"},
      t_values = {{t_destination}},
      t_type = {"table"},
    }
    checkActive(it_request.t_object)
    it_request.t_data = t_data
    newObject(it_request)
  end
  if b_mailbox then
    local _, _, s_mailbox_path_dst = getMailbox(t_destination, nil, true)
    if s_mailbox_path_src then
      local s_mailbox_move_command, n_ok = string.gsub(s_mailbox_move_command or tool.getRedisDefaultValue("config_mailbox_move_command"), "%${([^}]+)}", {MAILBOX = s_mailbox_path_src, DESTINATION = s_mailbox_path_dst})
      if n_ok == 2 then
        if os.execute(s_mailbox_move_command) then
          log.INFO("movUser: mailbox moved for: \"%s\" (%s) -> \"%s\" (%s)", it_request.t_object.s_object, s_mailbox_path_src, t_destination.s_object, s_mailbox_path_dst)
        else
          log.WARN("movUser: mailbox move failed for: \"%s\" (%s) -> \"%s\" (%s)", it_request.t_object.s_object, s_mailbox_path_src, t_destination.s_object, s_mailbox_path_dst)
        end
      else
        log.WARN("movUser: mailbox delete failed, invalid command: \"%s\"", s_mailbox_move_command)
      end
    else
      log.WARN("movUser: cannot read/move mailbox for: \"%s\"", it_request.t_object.s_object)
    end
  else
    log.DEBUG("movUser: no mailbox for: \"%s\"", it_request.t_object.s_object)
  end
  log.INFO("movUser: moved: \"%s\" => \"%s\"", it_request.t_object.s_object, t_destination.s_object)
  return tool.setResult(tool.t_config.t_status_code.ok)
end
tool.t_config.t_action.mov = movUser

local checkRequestContent = function(it_request)
  local log = tool.log
  local r_ro = tool.redis_ro
  local t_object = tool.checkUserDomain(it_request.object or "")
  it_request.t_object = t_object
  if t_object.s_cast and not(string.sub(t_object.s_type, 1, 1) == t_object.s_cast) then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("checkRequestContent: object invalid: %q", it_request.object or "nil"))
  end
  checkActive(t_object)
  log.DEBUG("checkRequestContent: got object: \"%s\" (%s)", t_object.s_object, t_object.s_type)
  if not(tool.t_config.t_action[it_request.action]) then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("action not supported: %q", it_request.action))
  end
  log.DEBUG("checkRequestContent: got action: \"%s\"", it_request.action)
  log.INFO("checkRequestContent: \"%s\" %s \"%s\" (%s)", it_request.t_sender.s_object, it_request.action, t_object.s_object, t_object.s_type)
  if t_object.s_type == "config" then
    if (it_request.action == "new") or (it_request.action == "del") then
      return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("action not supported for config object: \"%s\"", it_request.action))
    end
  end
  local t_user_admin_flag = r_ro.q({{"SMEMBERS", tool.getRedisKey("user_admin_flags", it_request.t_sender)}}, {true})[1]
  if t_user_admin_flag.read_only then
    if not(it_request.action == "get") then
      return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("invalid request with READ_ONLY user: %q", it_request.action))
    end
    log.DEBUG("checkRequestContent: READ_ONLY ADMIN FLAG: %s", it_request.t_sender.s_object)
  end
  -- check attribute permission
  t_object.s_request_permission = (t_object.s_type == "config") and "global_domain" or t_object.s_type
  -- set/get/new/mov
  if not(it_request.action == "del") then
    if (it_request.action == "new") and (t_object.s_type == "user") and (r_ro.q({{"GET", tool.getRedisKey("domain_alias", t_object)}})[1][1]) then
      return tool.setResult(tool.t_config.t_status_code.client, string.format("alias is set for domain: \"%s\"", t_object.s_domain))
    elseif (t_object.t_active.n_status == -1) and not(it_request.action == "new") and not(t_object.s_type == "config") then
      return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("checkRequestContent: %s \"%s\" does not exist", t_object.s_type, t_object.s_object))
    end
    local t_data = {
      t_redis_key = {},
      t_type = {},
      t_attributes = {}
    }
    if type(it_request.attributes) == "table" then
      t_data.t_attributes = it_request.attributes
      t_data.t_values = it_request.values
    else
      local b_success, loadFunction = pcall(load, string.format("return %s", it_request.attributes or ""))
      t_data.t_attributes = loadFunction() or {}
      b_success, loadFunction = pcall(load, string.format("return %s", it_request.values or ""))
      t_data.t_values = loadFunction()
    end
    it_request.t_data = t_data
    if (it_request.action == "mov") then
      if not(t_object.s_type == "user") then
        return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("checkRequestContent: object not supported: \"%s\" (%s)", t_object.s_object, t_object.s_type))
      end
      t_data.t_destination = tool.checkUserDomain(t_data.t_attributes and (type(t_data.t_attributes[1]) == "string") and t_data.t_attributes[1] or "")
      if not(t_data.t_destination.s_type == "user") then
        return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("checkRequestContent: destination not supported: \"%s\" (%s)", t_data.t_destination.s_object, t_data.t_destination.s_type))
      end
      checkActive(t_data.t_destination)
      t_data.t_destination.b_set_alias = t_data.t_attributes and t_data.t_attributes[2] and (type(t_data.t_attributes[2]) == "string") and (t_data.t_attributes[2] == "set_alias")
      log.DEBUG("checkRequestContent: mov \"%s\" -> \"%s\"%s", t_object.s_object, t_data.t_destination.s_object, t_data.t_destination.b_set_alias and " (set alias)" or "")
      t_data.t_attributes = {}
      t_object.s_request_permission = "domain"
    elseif 0 < #t_data.t_attributes then
      for n_k = 1, #t_data.t_attributes do
        if not(type(t_data.t_attributes[n_k]) == "string") then
          return tool.setResult(tool.t_config.t_status_code.client, "invalid attribute type")
        end
        if string.find(t_data.t_attributes[n_k], "active$") then
          if string.find(t_data.t_attributes[n_k], "_list_") then
            t_data.t_redis_key[n_k] = string.format("%s_%s", t_object.s_type, string.gsub(t_data.t_attributes[n_k], "_list_.*$", "_list"))
          elseif t_object.s_type == "user" then
            if t_data.t_attributes[n_k] == "autore_active" then
              t_data.t_redis_key[n_k] = "user_autore_start"
            else
              t_data.t_redis_key[n_k] = "domain_user_list"
            end
          elseif t_object.s_type == "domain" then
            t_data.t_redis_key[n_k] = "config_domain_list"
          elseif t_object.s_client then
            t_data.t_redis_key[n_k] = "config_client_list"
          else
            return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("checkRequestContent: attribute not supported: \"%s\"", t_data.t_attributes[n_k]))
          end
        elseif t_object.s_client then
          t_data.t_redis_key[n_k] = string.format("config_client_%s", t_data.t_attributes[n_k])
        else
          t_data.t_redis_key[n_k] = string.format("%s_%s", t_object.s_type, t_data.t_attributes[n_k])
        end
        if it_request.action == "get" then
          if not(t_user_admin_flag.read_pass) then
            if string.find(t_data.t_attributes[n_k], "pass$") then
              return tool.setResult(tool.t_config.t_status_code.client_auth, "attribute \"pass\" is WRITE_ONLY")
            end
          end
          if t_user_admin_flag.read_data then
            if string.find(t_data.t_attributes[n_k], "_data$") and (t_object.s_type == "user") then
              if t_data.t_attributes[n_k] == "recipient_data" then
                t_data.t_redis_key[n_k] = "domain_alias"
              elseif t_data.t_attributes[n_k] == "mailbox_data" then
                t_data.t_redis_key[n_k] = "user_mailbox"
              end
            end
          end
          if (t_data.t_attributes[n_k] == "user") or (t_data.t_attributes[n_k] == "domain") then
            if not(t_object.s_type == "user") and not(t_object.s_type == t_data.t_attributes[n_k]) then
              return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("checkRequestContent: attribute not supported: \"%s\"", t_data.t_attributes[n_k]))
            end
            t_data.t_redis_key[n_k] = string.format("%s_created", t_data.t_attributes[n_k])
          end
        end
        local t_redis_data = tool.t_config.t_redis_key[t_data.t_redis_key[n_k]]
        if not(t_redis_data) then
          return tool.setResult(tool.t_config.t_status_code.client, string.format("checkRequestContent: attribute not supported: \"%s\"", t_data.t_attributes[n_k]))
        end
        if tool.t_config.t_permission_role[t_object.s_request_permission] < tool.t_config.t_permission_role[t_redis_data[1]] then
          -- need higher permission?
          if not(it_request.action == "get") or (not(t_data.t_redis_key[n_k] == "config_online") and not(t_data.t_redis_key[n_k] == "user_admin_domains")) then
            t_object.s_request_permission = t_redis_data[1]
          end
        end
        t_data.t_type[n_k] = t_redis_data[2]
      end
      log.DEBUG("checkRequestContent: attributes are valid")
      -- check value data types
      if (it_request.action == "get") then
        t_data.s_option = t_data.t_values and (type(t_data.t_values[1]) == "string") and t_data.t_values[1]
      elseif (it_request.action == "set") or (it_request.action == "new") then
        if not(#t_data.t_redis_key == #t_data.t_values) then
          return tool.setResult(tool.t_config.t_status_code.client, "attribute value mismatch")
        end
        for n_k = 1, #t_data.t_values do
          if t_data.t_redis_key[n_k] == "config_client_list" then
            return tool.setResult(tool.t_config.t_status_code.client, string.format("attribute: \"%s\" is read_only - use (un)set client_pass to (de)activate client", t_data.t_attributes[n_k]))
          elseif (t_data.t_attributes[n_k] == "autore_active") or ((type(tool.t_config.t_redis_key[t_data.t_redis_key[n_k]][5]) == "nil") and not(t_data.t_attributes[n_k] == "active")) then
            return tool.setResult(tool.t_config.t_status_code.client, string.format("attribute: \"%s\" is read_only", t_data.t_attributes[n_k]))
          end
          if not(type(t_data.t_values[n_k]) == tool.t_config.t_redis_key[t_data.t_redis_key[n_k]][2]) then
            if not((tool.t_config.t_redis_key[t_data.t_redis_key[n_k]][2] == "number") and tonumber(t_data.t_values[n_k])) then
              return tool.setResult(tool.t_config.t_status_code.client, string.format("value for attribute: \"%s\" not supported", t_data.t_attributes[n_k]))
            end
          end
          if type(t_data.t_values[n_k]) == "table" then
            for n_t_k, s_t_v in pairs(t_data.t_values[n_k]) do
              s_t_v = (type(s_t_v) == "string") and s_t_v or ""
              if not(s_t_v == "+") and not(s_t_v == "-") then
                local b_accept = true
                t_data.t_values[n_k][n_t_k] = tool.checkUserDomain(s_t_v)
                if not(t_data.t_values[n_k][n_t_k].s_object) and ((t_data.t_redis_key[n_k] == "user_aliases") or (string.find(t_data.t_redis_key[n_k], "^config_admin_flag_.+_users$"))) then
                  b_accept = false
                elseif (t_data.t_values[n_k][n_t_k].s_object == t_object.s_object) and (t_data.t_redis_key[n_k] == "user_aliases") then
                  b_accept = false
                elseif not(t_data.t_values[n_k][n_t_k].s_domain) and (t_data.t_redis_key[n_k] == "user_admin_domains") then
                  b_accept = false
                elseif t_data.t_redis_key[n_k] == "user_admin_flags" then
                  if not(tool.t_config.t_admin_flag[s_t_v]) then
                    b_accept = false
                  else
                    t_data.t_values[n_k][n_t_k].s_object = s_t_v
                  end
                elseif string.find(t_data.t_attributes[n_k], "^admin_flag_") then
                  if (r_ro.q({{"SISMEMBER", tool.getRedisKey("domain_user_list", t_data.t_values[n_k][n_t_k]), t_data.t_values[n_k][n_t_k].s_object}})[1][1] == 0) then
                    return tool.setResult(tool.t_config.t_status_code.client, string.format("value: \"%s\" is no valid user", t_data.t_values[n_k][n_t_k].s_object))
                  end
                elseif t_data.t_redis_key[n_k] == "user_restricted_clients" then
                  t_data.t_values[n_k][n_t_k].s_object = string.gsub(s_t_v, "@$", "")
                else
                  t_data.t_values[n_k][n_t_k].s_object = s_t_v
                end
                if not(b_accept) then
                  return tool.setResult(tool.t_config.t_status_code.client, string.format("value: \"%s\" for attribute: \"%s\" not supported", s_t_v, t_data.t_attributes[n_k]))
                end
              end
            end
          elseif (t_data.t_redis_key[n_k] == "config_mailbox_size_cache_interval") or string.find(t_data.t_redis_key[n_k], "limit_?[mw]?a?[xr]?n?$") then
            local n_value, _ = tonumber(t_data.t_values[n_k])
            if not(n_value) then
              local s_convert = "size"
              if (t_data.t_redis_key[n_k] == "config_mailbox_size_cache_interval") then
                s_convert = "time"
              end
              _, n_value = tool.convertUnit(s_convert, t_data.t_values[n_k])
            end
            if not(n_value) then
              return tool.setResult(tool.t_config.t_status_code.client, string.format("value: \"%s\" for attribute: \"%s\" not supported", t_data.t_values[n_k], t_data.t_attributes[n_k]))
            end
            t_data.t_values[n_k] = (n_value == 0) and "0" or n_value
          elseif (t_data.t_attributes[n_k] == "autore_start") or (t_data.t_attributes[n_k] == "autore_end") then
            local n_autore_date = tonumber(t_data.t_values[n_k])
            if n_autore_date and ((100000000 < n_autore_date) or (n_autore_date < tonumber(os.date("!%Y%m%d")))) then
              return tool.setResult(tool.t_config.t_status_code.client, string.format("value: \"%s\" for attribute: \"%s\" not supported", t_data.t_values[n_k], t_data.t_attributes[n_k]))
            end
          elseif (t_data.t_attributes[n_k] == "autore_reply_as") then
            t_data.t_values[n_k] = tool.checkUserDomain(t_data.t_values[n_k])
            if not(t_data.t_values[n_k].s_object) then
              return tool.setResult(tool.t_config.t_status_code.client, string.format("value: \"%s\" for attribute: \"%s\" not supported", t_data.t_values[n_k].s_object, t_data.t_attributes[n_k]))
            end
          elseif string.find(t_data.t_attributes[n_k], "pass$") then
            local t_password = r_ro.q({{"GET", tool.getRedisKey("config_password_min_length")}, {"SMEMBERS", tool.getRedisKey("config_password_condition")}})
            if not((t_data.t_redis_key[n_k] == "config_client_pass") and (#t_data.t_values[n_k] == 0)) then
              if #t_data.t_values[n_k] < tonumber(t_password[1][1] or tool.getRedisDefaultValue("config_password_min_length")) then
                return tool.setResult(tool.t_config.t_status_code.client, string.format("value length < %d is not supported for attribute: \"%s\"", tonumber(t_password[1][1] or tool.getRedisDefaultValue("config_password_min_length")), t_data.t_attributes[n_k]))
              end
              for n_cond = 1, #t_password[2] do
                if not(string.find(t_data.t_values[n_k], t_password[2][n_cond])) then
                  return tool.setResult(tool.t_config.t_status_code.client, string.format("value condition failed for attribute: \"%s\"", t_data.t_attributes[n_k]))
                end
              end
            end
          elseif (t_data.t_attributes[n_k] == "mailbox_type") then
            t_data.t_values[n_k] = string.lower(t_data.t_values[n_k])
            if not(string.find(string.format("@%s@@", table.concat(t_mailbox_type, "@")), string.format("@%s@", t_data.t_values[n_k]))) then
              return tool.setResult(tool.t_config.t_status_code.client, string.format("value condition failed for attribute: \"%s\"", t_data.t_attributes[n_k]))
            end
          elseif t_data.t_redis_key[n_k] == "domain_alias" then
            if not(t_data.t_values[n_k] == "") then
              if t_object.s_domain == t_data.t_values[n_k] then
                return tool.setResult(tool.t_config.t_status_code.client, string.format("domain and alias may not be the same: \"%s\"", t_data.t_values[n_k]))
              end
              local t_domain_data = r_ro.q({{"SCARD", tool.getRedisKey("domain_user_list", it_request.t_object)}, {"GET", (tool.getRedisKey("domain_created", {s_domain = t_data.t_values[n_k]}))}})
              if 0 < t_domain_data[1][1] then
                return tool.setResult(tool.t_config.t_status_code.client, string.format("user(s) exist for domain: \"%s\"", t_object.s_domain))
              elseif not(t_domain_data[2][1]) then
                return tool.setResult(tool.t_config.t_status_code.client, string.format("alias domain: \"%s\" does not exist", t_data.t_values[n_k]))
              end
            end
          end
        end
      end
      log.DEBUG("checkRequestContent: values are valid")
    end
  else
    it_request.t_data = {}
  end
  if t_object.s_domain == "localhost" then
    t_object.s_request_permission = "config"
  end
  -- max required perm for transaction is: t_object.s_request_permission
  log.DEBUG("checkRequestContent: require: \"%s\"", t_object.s_request_permission)
  if (tool.t_config.t_permission_role[t_object.s_request_permission] < tool.t_config.t_permission_role.user) or (tool.t_config.t_permission_role.config < tool.t_config.t_permission_role[t_object.s_request_permission]) then
    -- should never happen - we're out of range
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("PERMISSION DENIED: %s -> %s", it_request.t_sender.s_object, t_object.s_request_permission))
  end
  it_request.t_sender.s_has_permission = t_object.s_request_permission
  -- user self
  if (tool.t_config.t_permission_role[t_object.s_request_permission] == tool.t_config.t_permission_role.user) and (it_request.t_sender.s_object == t_object.s_object) then
    return tool.setResult(tool.t_config.t_status_code.ok_auth, string.format("Permission allowed: %s (self) -> %s (%s)", it_request.t_sender.s_object, t_object.s_object, t_object.s_request_permission))
  end
  -- config admin (full access)
  if t_user_admin_flag.config then
    it_request.t_sender.s_has_permission = tool.t_config.t_admin_flag.config
    return tool.setResult(tool.t_config.t_status_code.ok_auth, string.format("Permission allowed: %s (config admin) -> %s (%s)", it_request.t_sender.s_object, t_object.s_object, t_object.s_request_permission))
  elseif tool.t_config.t_permission_role[t_object.s_request_permission] == tool.t_config.t_permission_role.config then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("PERMISSION DENIED: %s -> %s (%s)", it_request.t_sender.s_object, t_object.s_object, t_object.s_request_permission))
  end
  -- global domain admin (domain access)
  if t_user_admin_flag.global_domain then
    it_request.t_sender.s_has_permission = tool.t_config.t_admin_flag.global_domain
    return tool.setResult(tool.t_config.t_status_code.ok_auth, string.format("Permission allowed: %s (global_domain admin) -> %s (%s)", it_request.t_sender.s_object, t_object.s_object, t_object.s_request_permission))
  elseif tool.t_config.t_permission_role[t_object.s_request_permission] == tool.t_config.t_permission_role.global_domain then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("PERMISSION DENIED: %s (global_domain admin) -> %s (%s)", it_request.t_sender.s_object, t_object.s_object, t_object.s_request_permission))
  end
  -- domain admin (user access)
  if r_ro.q({{"SISMEMBER", tool.getRedisKey("user_admin_domains", it_request.t_sender), t_object.s_domain}})[1][1] == 1 then
    if not(it_request.action == "mov") or r_ro.q({{"SISMEMBER", tool.getRedisKey("user_admin_domains", it_request.t_sender), it_request.t_data.t_destination.s_domain}})[1][1] == 1 then
      return tool.setResult(tool.t_config.t_status_code.ok_auth, string.format("Permission allowed: %s (domain admin) -> %s (%s)", it_request.t_sender.s_object, it_request.t_data.t_destination and string.format("%s=>%s", t_object.s_object, it_request.t_data.t_destination.s_object) or t_object.s_object, t_object._request_permission))
    end
  end
  -- looser
  return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("PERMISSION DENIED: %s -> %s (%s)", it_request.t_sender.s_object, it_request.t_data.t_destination and string.format("%s=>%s", t_object.s_object, it_request.t_data.t_destination.s_object) or t_object.s_object, t_object.s_request_permission))
end

local checkRequestEnvelope = function(it_request, is_request, is_hash, is_remote_addr)
  local log = tool.log
  local t_sender = tool.checkUserDomain(it_request.ruser or "")
  it_request.t_sender = t_sender
  if not(t_sender.s_object) then
    return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: no valid user data")
  end
  t_sender.s_client = string.gsub(it_request.rclient, "@$", "")
  log.INFO("checkRequestEnvelope: %s@%s @ %s[%s]", t_sender.s_user, t_sender.s_domain, t_sender.s_client, is_remote_addr or "127.0.0.1")
  checkActive(t_sender)
  if t_sender.t_active.n_domain == -1 then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("ACCESS DENIED: domain \"%s\" does not exist", t_sender.s_domain))
  elseif t_sender.t_active.n_domain == 0 then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("ACCESS DENIED: domain \"%s\" deactivated", t_sender.s_domain))
  end
  if t_sender.t_active.n_user == -1 then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("ACCESS DENIED: user \"%s\" does not exist", t_sender.s_object))
  elseif t_sender.t_active.n_user == 0 then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("ACCESS DENIED: user \"%s\" deactivated", t_sender.s_object))
  end
  if t_sender.t_active.b_restricted_client then
    return tool.setResult(tool.t_config.t_status_code.client_auth, string.format("ACCESS DENIED: user \"%s\" is not allowed to use client \"%s\"", t_sender.s_object, t_sender.s_client))
  end
  local t_check_data = tool.mgetRedis({"config_online", "config_client_pass", "config_client_ip", "user_pass", "internal_user_rid"}, t_sender)
  if not(t_check_data["config_online"]) and not((type(it_request.attributes) == "table") and (it_request.attributes[1] == "online")) and not((type(it_request.attributes) == "string") and (string.find(it_request.attributes, "online"))) then
    return tool.setResult(tool.t_config.t_status_code.server, "ACCESS DENIED: server is offline")
  end
  if not(t_check_data["config_client_pass"]) then
    return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: client not found")
  end
  local t_pass = {}
  if is_remote_addr then
    t_pass.s_client = tool.crypt(t_check_data["config_client_pass"], tool.t_config.DATA_MASTER_PASSWORD, tool.DECRYPT, "x")
    log.DEBUG("checkRequestEnvelope: got client pass")
    if t_check_data["config_client_ip"] then
      if not(t_check_data["config_client_ip"] == is_remote_addr) then
        return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: client IP invalid")
      end
      log.DEBUG("checkRequestEnvelope: client IP valid")
    end
  else
    if not(t_check_data["config_client_pass"] == tool.t_config.client_pass) then
      return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: client pass invalid")
    end
  end
  if not(t_check_data["user_pass"]) then
    return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: user not found")
  end
  if is_remote_addr then
    t_pass.s_user = tool.crypt(t_check_data["user_pass"], tool.t_config.DATA_MASTER_PASSWORD, tool.DECRYPT, "x")
    log.DEBUG("checkRequestEnvelope: got user pass")
  else
    if not(t_check_data["user_pass"] == tool.t_config.s_user_pass) then
      return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: user pass invalid")
    end
  end
  -- Lua Version < 5.3
  if 16 < #it_request.rid then
    return tool.setResult(tool.t_config.t_status_code.client_auth, "ACCESS DENIED: request id is not allowed to have more than 16 bit")
  end
  if (tonumber(it_request.rid) or 0) <= (tonumber(t_check_data["internal_user_rid"]) or 1) then
    return tool.setResult(tool.t_config.t_status_code.client_auth_tmp, string.format("ACCESS DENIED: request id invalid: %d (last: %d)", tonumber(it_request.rid) or 0, tonumber(t_check_data["internal_user_rid"]) or 1))
  end
  if is_remote_addr then
    local s_hash, s_err = tool.hash(is_request, it_request.rhash_algo, string.format("%s%s", t_pass.s_client, t_pass.s_user), it_request.rhash_armor)
    if not(s_hash) or not(s_hash == is_hash) then
      local s_result = s_hash and "hash mismatch" or (s_err or "cannot create hash")
      local error_code = s_hash and tool.t_config.t_status_code.client_auth or tool.t_config.t_status_code.server
      return tool.setResult(error_code, string.format("ACCESS DENIED: %s (server: %s / client: %s)", s_result, s_hash or "", is_hash))
    end
  end
  tool.redis.q({{"MSET", tool.getRedisKey("internal_user_rid", t_sender), it_request.rid, tool.getRedisKey("user_last_access", t_sender), os.date("!%Y-%m-%dT%H:%MZ")}})
  return tool.setResult(tool.t_config.t_status_code.ok_auth, string.format("Access allowed: %s@%s @ %s[%s]", t_sender.s_user, t_sender.s_domain, t_sender.s_client, is_remote_addr or ""))
end

_M.doRequest = function(it_request, is_request, is_hash, is_remote_addr)
  local t_result = checkRequestEnvelope(it_request, is_request, is_hash, is_remote_addr)
  if t_result.status == 0 then
    return t_result
  end
  t_result = checkRequestContent(it_request)
  if (t_result.status == 0) then
    return t_result
  end
  return tool.t_config.t_action[it_request.action](it_request)
end

_M.doCGIRequest = function(i_scgi_sock_io)
  local log = tool.log
  log.DEBUG("doCGIRequest: HTTP REQUEST")
  local t_cgi_env = {
    {"REQUEST_METHOD", "REQUEST_METHOD", nil},
    {"REMOTE_ADDR", "REMOTE_ADDR", nil},
    {"CONTENT_TYPE", "CONTENT_TYPE", nil},
  }
  local cgi_ctx = tool.require("cgi").init(tool.cgi_log, t_cgi_env, i_scgi_sock_io)
  if not(tool.connectRedis()) then
    return
  end
  local json = tool.require("json")
  if (cgi_ctx:getEnv("REQUEST_METHOD") == "POST") then
    if not(string.lower(cgi_ctx:getEnv("CONTENT_TYPE")) == "application/x-www-form-urlencoded") then
      cgi_ctx:write(json.encode(setResult(tool.t_config.t_status_code.client, "no valid content type")))
      return
    end
  elseif not(cgi_ctx:getEnv("REQUEST_METHOD") == "GET") or (tool.redis_ro.q({{"EXISTS", tool.getRedisKey("config_enable_cgi_request_method_get")}})[1][1] == 0) then
    cgi_ctx:write(json.encode(setResult(tool.t_config.t_status_code.client, "no valid request method")))
    return
  end
  local s_request = cgi_ctx:getData("request")
  log.INFO("doCGIRequest: START Request: %s", s_request)
  log.DEBUG("request: \"%s\"", s_request)
  local s_hash = cgi_ctx:getData("hash")
  log.DEBUG("hash: \"%s\"", s_hash)
  cgi_ctx:setContentType("text/plain")
  local t_request = json.decode(s_request)
  if not(type(t_request) == "table") then
    cgi_ctx:write(json.encode(tool.setResult(tool.t_config.t_status_code.client, "no valid request data")))
    return
  end
  log.DEBUG("doCGIRequest: got valid json request")
  local t_result = _M.doRequest(t_request, s_request, s_hash, cgi_ctx:getEnv("REMOTE_ADDR"))
  tool.closeRedis()
  local s_json_result = json.encode(t_result)
  log.INFO("doCGIRequest: END Request: %s", string.gsub(s_json_result, "pass\": \"[^ ]* ", "pass\": \"***\" "))
  cgi_ctx:write(s_json_result)
end

return _M

